"""
FILL IN LATER
"""

from world import pokedex, rules
from random import randint
from typeclasses import characters
from evennia.utils.create import create_object

def create_pokemon(name, location = None, level = 1, iv = None, ev = None, owner = None):
    
    # Create Pokemon and run a stat check, raising an error if it fails.
    pokemon = create_object(typeclass=characters.BasePokemon,
                            key=name,
                            location=location)
    pokemon.db.level = level
    pokemon.db.iv = iv
    pokemon.db.ev = ev
    pokemon.db.owner = owner
            
    try:
        stat_check(pokemon, rebuild=True)
    except AttributeError as err:
        # Delete faulty pokemon and explain why.
        pokemon.delete()
        raise AttributeError(err)
        
    return pokemon
                    
            
def stat_check(pokemon, rebuild=False):
    """
    To be called whenever stats are to be determined/checked. Will
    replace bad or insane values with appropriate ones and recaculate
    stats.
    
    If rebuild true it will alter pokemon to make sure it conforms.
    """
    
    # Check if Pokemon.
    if not isinstance(pokemon, characters.BasePokemon):
        string = "Command aborted!\n"
        string += "Not provided object of Pokemon typeclass."
        raise AttributeError(string)
    
    # Check if valid key.
    if pokemon.key not in pokedex.POKEDEX:
        string = "Command aborted!\n"
        string += "Pokemon: '%s' not found.\nPokemon.key must be the name "
        string += "of a valid Pokemon."
        raise AttributeError(string % pokemon.key)

    # Check if level exists, if valid value and in valid range.
    if not pokemon.attributes.has("level"):
        pokemon.db.level = 1
        
    if not isinstance(pokemon.db.level, (int, long)):
        pokemon.db.level = 1
    
    if pokemon.db.level < 1:
        pokemon.db.level = 1
    if pokemon.db.level > 100:
        pokemon.db.level = 100
    
    # EXP CHECKS
    
    # Check if iv exists, is list, is correct length and valid values
    if not pokemon.attributes.has("iv"):
        pokemon.db.iv = [randint(0, 15), randint(0, 15), randint(0, 15),
                         randint(0, 15), randint(0, 15)]
    
    if not isinstance(pokemon.db.iv, (list, tuple)):
        pokemon.db.iv = [randint(0, 15), randint(0, 15), randint(0, 15),
                         randint(0, 15), randint(0, 15)]
    
    if not len(pokemon.db.iv) == 5:
        if len(pokemon.db.iv) < 5:
            for i in range(5-len(pokemon.db.iv)):
                pokemon.db.iv.append(randint(0, 15))
        if len(pokemon.db.iv) > 5:
            for i in range(-5+len(pokemon.db.iv)):
                del pokemon.db.iv[-1]
    
    for i in range(len(pokemon.db.iv)):
        if not isinstance(pokemon.db.iv[i], (int, long)):
            pokemon.db.iv[i] = randint(0, 15)
        if i < 0 or i > 15:
            pokemon.db.iv[i] = randint(0, 15)

    # Check if ev exists, is list, is correct length and valid values
    if not pokemon.attributes.has("ev"):
        pokemon.db.ev = [0, 0, 0, 0, 0]
    
    if not isinstance(pokemon.db.ev, (list, tuple)):
        pokemon.db.ev = [0, 0, 0, 0, 0]
    
    if not len(pokemon.db.ev) == 5:
        if len(pokemon.db.ev) < 5:
            for i in range(5-len(pokemon.db.ev)):
                pokemon.db.ev.append(0)
        if len(pokemon.db.ev) > 5:
            for i in range(-5+len(pokemon.db.ev)):
                del pokemon.db.ev[-1]
    
    for i in range(len(pokemon.db.ev)):
        if not isinstance(pokemon.db.ev[i], (int, long)):
            pokemon.db.ev[i] = 0
        if i < 0 or i > 65535:
            if pokemon.db.ev < 1:
                pokemon.db.ev = 0
            if pokemon.db.ev > 65535:
                pokemon.db.ev = 65535
               
    # GENDER CHECKS
