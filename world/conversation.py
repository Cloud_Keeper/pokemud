# encoding=utf-8
"""

Holds conversations.

Many of these are just a single response with no player input, but others will
be more complicated and I want to treat them all the same.

"""
from utils import poketext
from evennia.utils.evmenu import EvMenuCmdSet

# PALLET TOWN CONVERSATIONS


def pallet_home(caller):

    string = ("Right. All boys leave home some day. It said so on TV. "
              "\nPROF.OAK, next door, is looking for you.")
    text = poketext.nobordertext(string, "Mum")

    caller.cmdset.remove(EvMenuCmdSet)
    return text, None


def pallet_house(caller):

    string = ("Hi " + caller.key + "! BLUE is out at Grandpa's lab.")
    text = poketext.nobordertext(string)

    caller.cmdset.remove(EvMenuCmdSet)
    return text, None


def pallet_riverbank(caller):

    string = ("I'm raising POKEMON too! "
              "\nWhen they get strong, they can protect me!")
    text = poketext.nobordertext(string)

    caller.cmdset.remove(EvMenuCmdSet)
    return text, None


def pallet_oslab(caller):

    string = ("Technology is incredible! "
              "\nYou can now store and recall items and POKEMON as data via PC!")
    text = poketext.nobordertext(string)

    caller.cmdset.remove(EvMenuCmdSet)
    return text, None


def pallet_lab1(caller):

    string = ("I study POKEMON as PROF.OAK'S AIDE.")
    text = poketext.nobordertext(string)

    caller.cmdset.remove(EvMenuCmdSet)
    return text, None


def pallet_lab2(caller):

    string = ("PROF.OAK is the authority on POKEMON! "
              "\nMany POKEMON trainers hold him in high regard!")
    text = poketext.nobordertext(string)

    caller.cmdset.remove(EvMenuCmdSet)
    return text, None

