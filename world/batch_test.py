
# -*- coding: utf-8 -*-

#
# Batchcode Script
#

# HEADER

# CODE
from evennia import create_object
from typeclasses.objects import Equippable, Consumable
from typeclasses.objects import InfinityItem
# from typeclasses import basicnpc

# # Testing conversations
# npc = create_object(basicnpc.BasicNPC, key="man", location=caller.location)
# npc.db.conversation = "start_node"

# Testing code for character create.
# shirt = create_object(InfinityItem, key="test", location=caller.location)

apple = create_object(Consumable, key="red delicious", aliases=["apple"],
                      location=caller.location)

jeans = create_object(Equippable, key="blue jeans", aliases=["jeans"],
                      location=caller.location)
jeans.db.slots = ["legs"]
jeans.db.desc = "test jeans"
#
# hat = create_object(Equippable, key="red baseball cap", aliases=["cap"],
#                     location=caller.location)
# hat.db.slots = ["head"]
# hat.db.desc = "test hat"

#                 # This is where the work occurs. Because the add command
#                 # returns True or False on success it is wrapped in an if
#                 # supplying a message if it fails.
#                 if not caller.equip.add(obj):
#                     caller.msg("You can't {} {}.".format(action, obj.name))
#                     return
