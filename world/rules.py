# encoding=utf-8
"""

Holds functions which are used by other modules to implement the games
consistent rules.

We've decied on Gen 3 and the most balanced Gen.

"""

import math

# Pokemon Game Resources

BADGES = ["⎔", "💧", "❂", "❁", "♥", "⌾", "⟒", "⚚"]


# Pokemon Generation Rules

def calculate_stat(basestat, individualvalue, effortvalue, level, nature=1):
    """Calculates pokemon stats"""
    
    return math.floor(math.floor((2 * basestat + individualvalue
                      + effortvalue) * level / 100 + 5) * nature)

def calculate_health(basestat, individualvalue, effortvalue, level, nature=1):
    """Calculates pokemon stats"""
    
    return math.floor((2 * basestat + individualvalue
                      + effortvalue) * level / 100 + level + 10)
