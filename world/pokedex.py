"""

The pokedex holds all hardcoded values for pokemon.

bstat = HP, ATT, DEF, SPATT, SPDEF, SPEED

"""
POKEDEX = {

    "Bulbasaur": {
        "index": 001,
        "classification": "Seed Pokemon",
        "height": 0.7,
        "weight": 6.9,
        "body": (("head", ("head", "face", "neck")),
                 ("body", ("body", "bulb")),
                 ("left_foreleg", ("left_foreleg", "left_foreclaw")),
                 ("right_foreleg", ("right_foreleg", "right_foreclaw")),
                 ("left_rearleg", ("left_rearleg", "left_rearclaw")),
                 ("right_rearleg", ("right_rearleg", "right_rearclaw")),),
        "description": ("A small, quadruped Pokemon that has blue-green skin "
                        "with darker green patches. It has red eyes with white"
                        " pupils and scleras and pointed, ear-like structures "
                        "on top of its head. Its snout is short and blunt, and"
                        " it has a wide mouth. A pair of small, pointed teeth "
                        "are visible in the upper jaw when its mouth is open. "
                        "Each of its thick legs ends with three sharp claws. "
                        "On its back is a green plant bulb, which appears to "
                        "be part of the Pokemon."),
        "entries": [
            "A strange seed was planted on its back at birth. The plant sprouts and grows with this Pokemon.",
            "It can go for days without eating a single morsel. In the bulb on its back, it stores energy.",
            "The seed on its back is filled with nutrients. The seed grows steadily larger as its body grows.",
            "It carries a seed on its back right from birth. As it grows older, the seed also grows larger.",
            "While it is young, it uses the nutrients that are stored in the seeds on its back in order to grow.",
            "They can be seen napping in bright sunlight. There is a seed on its back. By soaking up the sun's rays, the seed grows progressively larger.",
            "There is a plant seed on its back right from the day this Pokemon is born. The seed slowly grows larger.",
            "A strange seed was planted on its back at birth. The plant sprouts and grows with this Pokemon.",
            "For some time after its birth, it grows by gaining nourishment from the seed on its back.",
            "The seed on its back is filled with nutrients. The seed grows steadily larger as its body grows.",
            "It carries a seed on its back right from birth. As it grows older, the seed also grows larger.",
            "For some time after its birth, it grows by gaining nourishment from the seed on its back.",
            "A strange seed was planted on its back at birth. The plant sprouts and grows with this Pokemon.",
            "For some time after its birth, it grows by gaining nourishment from the seed on its back.",
        ],

        "stat": (45, 49, 49, 65, 65, 45),
        "type": ("Grass", "Poison"),
        "ability": ("Overgrow"),
        "hidden_ability": "Chlorophyll",
        "learn_set": {
            1: "Tackle",
            4: "Growl",
            7: "Leech Seed",
            10: "Vine Whip",
            15: ("Poison Powder", "Sleep Powder"),
            20: "Razor Leaf",
            25: "Sweet Scent",
            32: "Growth",
            39: "Synthesis",
            46: "SolarBeam",
            },

        "gender_ratio": 87.5,
        "catch_rate": 45,
        "experience_yield": 64,
        "leveling_rate": "Medium Slow",
        "egg_group": ("Monster", "Grass"),
        "hatch_time": (5397, 5653)
        }
    }
