
# -*- coding: utf-8 -*-

"""
Pallet Town Batchcode
          ▲▲
         Θ☼☼Θ
ΘΘΘΘΘΘΘΘΘΘ☼☼ΘΘΘΘΘΘΘΘ
Θ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓Θ
Θ▓▒▒┌──┐▒▒1▒┌──┐▒▒▓Θ
Θ▓▒▒╞══╡▒▒▒▒╞══╡▒▒▓Θ
Θ▓▒□└■─┘▒▒▒□└■─┘▒▒▓Θ
Θ▓▒▒▒2▒▒▒▒▒▒▒3▒▒▒▒▓Θ
Θ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓Θ
Θ▓▒▒▒▒▒▒▒▒┌────┐▒▒▓Θ
Θ▓▒▒πππ□▒▒╞════╡▒▒▓Θ
Θ▓▒▒▓▓▓▓▒▒│    │▒▒▓Θ
Θ▓▒▒▓▓▓▓▒▒└─■──┘▒▒▓Θ
Θ▓▒▒▒4▒▒▒▒▒▒5▒▒▒▒▒▓Θ
Θ▓▒▒▒▒▒▒▒▒πππ□ππ▒▒▓Θ
Θ▓▓▓≈≈≈≈▒▒▓▓▓▓▓▓▒▒▓Θ
Θ▓▓▓≈6≈≈▒▒▓▓7▓▓▓▒▒▓Θ
Θ▓▓▓≈≈≈≈▒▒▒▒▒▒▒▒▒▒▓Θ
ΘΘ▓▓≈≈≈≈ΘΘΘΘΘΘΘΘΘΘΘΘ
  ▼▼▼▼▼▼

"""

from evennia import create_object
from typeclasses import rooms, exits, characters

# INITIAL AREA SETUP

# 1. Pallet Town Crossroads
pallet_crossroads = create_object(rooms.Room, key="Pallet Town Crossroads")

# 2. Pallet Town Home
pallet_oshome = create_object(rooms.Room, key="Outside Home")
pallet_home = create_object(rooms.Room, key="Home")

# 3. Pallet Town House
pallet_oshouse = create_object(rooms.Room, key="Outside House")
pallet_house = create_object(rooms.Room, key="House")

# 4. Pallet Town River Bank
pallet_riverbank = create_object(rooms.Room, key="Pallet Town River Bank")

# 5. Oak's Laboratory
pallet_oslab = create_object(rooms.Room, key="Outside Laboratory")
pallet_lab = create_object(rooms.Room, key="Laboratory Atrium")
pallet_oak = create_object(rooms.Room, key="Professor Oak's Office")
pallet_corral = create_object(rooms.Room, key="Laboratory Corral")

# 6. Pallet Town River
pallet_river = create_object(rooms.Room, key="Pallet Town River")

# 7. Pallet Town Park
pallet_park = create_object(rooms.Room, key="Pallet Town Park")


# COMPLETE AREA SETUP


"""
1. Pallet Town Crossroads
       ▲▲
      Θ☼☼Θ
    ΘΘΘ☼☼ΘΘ
    ▒▒▒▒▒▒▒
    ┐▒▒1▒┌─
    ╡▒▒▒▒╞═
    ┘▒▒▒□└■
    ◄▒▒▒▒▒►
    ◄▒▒▒▒▒►
     ▼▼▼▼▼

"""

pallet_crossroads.db.desc = ("Diverging into a crossroad, a golden soil road "
                             "serves as the main thoroughfare of the small "
                             "village nestled in the densely forested country "
                             "side visible in all directions. The edges of "
                             "the road are cleanly cut into a carpet of soft "
                             "green grass and lined with shrubs and bushes "
                             "that, unlike the dense forest that surrounds "
                             "the town, appear tame and well tended. The red "
                             "tiled roofs of nearby houses to the east and "
                             "west are just visible over the greenery whilst "
                             "a clean white sign stands idly nearby. Towards "
                             "the north the road appears to have become "
                             "overgrown by tall grass, whilst to the south a "
                             "large building is visible in the distance.")

pallet_crossroads.db.details["sign"] = ("Standing on the side of the road, "
                                        "the white sign displays it's message "
                                        "in clear black text:\nPALLET TOWN\n"
                                        "Shades of your journey await!")
pallet_crossroads.db.details["road"] = ("The compacted soil road appears to "
                                        "have it's surface routinely stamped "
                                        "all over by a large heavy Pokemon to "
                                        "keep it's surface firm.")
pallet_crossroads.db.details["shrubs"] = ("The plants look to be flourishing "
                                          "under the consistent care and "
                                          "attention of the local residents.")
pallet_crossroads.db.details["bushes"] = ("The plants look to be flourishing "
                                          "under the consistent care and "
                                          "attention of the local residents.")
pallet_crossroads.db.details["forest"] = ("The rolling hills and mountains "
                                          "which surround the small town are "
                                          "forested with thick green trees. "
                                          "These untamed places look unsafe "
                                          "for anyone but the best pokemon "
                                          "trainers.")

crossroads_ex1 = create_object(exits.Exit, key="West",
                               aliases=["w", "west"],
                               location=pallet_crossroads,
                               destination=pallet_oshome)
crossroads_ex2 = create_object(exits.Exit, key="South",
                               aliases=["s", "south"],
                               location=pallet_crossroads,
                               destination=pallet_riverbank)
crossroads_ex3 = create_object(exits.Exit, key="East",
                               aliases=["e", "east"],
                               location=pallet_crossroads,
                               destination=pallet_oshouse)


"""
2. Pallet Town Home
    Θ▓▒▒┌──┐▒▒
    Θ▓▒▒╞══╡▒▒
    Θ▓▒□└■─┘▒▒
    Θ▓▒▒▒2▒▒▒►
    Θ▓▒▒▒▒▒▒▒►
    Θ▓▒▒▒▒▒▒▒►
    Θ▓▒▒πππ□▒▒

"""

pallet_oshome.db.desc = ("A white picket fence surrounds a comfortable piece "
                         "of land but only appears to exclude a nearby road, "
                         "as the green shrubs and bushes which extend in all "
                         "directions continue unhindered within its bounds. "
                         "Standing in contrast to the greenery, a red tiled "
                         "roof stands atop the clean white weatherboard walls "
                         "of a simple two storey house. Warm and inviting, "
                         "stone steps mark the path between the compact soil "
                         "road and a white front door, whilst on either side "
                         "lush gardens show the promise of returning the care "
                         "and warmth which has obviously been shown to them. "
                         "To the east the road can be seen to extend into "
                         "a crossroads.")

pallet_oshome.db.details["fence"] = ("The clean white picket fence continues "
                                     "along the outside borders of the "
                                     "property. Like everything else, it "
                                     "appears to be maintained with regular "
                                     "care.")
pallet_oshome.db.details["road"] = ("The compacted soil road appears to "
                                    "have it's surface routinely stamped "
                                    "all over by a large heavy Pokemon to "
                                    "keep it's surface firm.")
pallet_oshome.db.details["shrubs"] = ("The plants look to be flourishing "
                                      "under the consistent care and "
                                      "attention of the local residents.")
pallet_oshome.db.details["bushes"] = ("The plants look to be flourishing "
                                      "under the consistent care and "
                                      "attention of the local residents.")
pallet_oshome.db.details["garden"] = ("Various herbs and vegetables are neatly"
                                      " arranged in small soil plots. The "
                                      "plants are lush with healthy colour. "
                                      "Good enough to eat.")

oshome_ex1 = create_object(exits.Exit, key="East", aliases=["e", "east"],
                           location=pallet_oshome,
                           destination=pallet_crossroads)
oshome_ex2 = create_object(exits.Exit, key="Enter", aliases=["enter"],
                           location=pallet_oshome,
                           destination=pallet_home)
                
pallet_home.db.desc = ("Simply exuding warmth, the ground floor of the two "
                       "storey building encompasses the dining and living "
                       "areas of the residence. In the centre of the room "
                       "stands a table with chairs arranged neatly around it. "
                       "Positioned on the table, a vase flush with freshly "
                       "picked wildflowers fills the room with fragrance. "
                       "Towards the centre of the rear wall a television sits "
                       "whilst to it’s right stands a series of bookshelves "
                       "and to it’s left a set of stairs leading to the "
                       "second storey of the house.")

pallet_home.db.details["table"] = ("A sturdy wooden table standing in the "
                                   "centre of the room. Chairs are neatly "
                                   "arranged around it and on top sits a "
                                   "vase of wildflowers.")
pallet_home.db.details["vase"] = ("A plain purple vase filled with flowers."
                                  "The flowers appear to have been freshly "
                                  "picked from the wildflowers that can be "
                                  "found around the town.")
pallet_home.db.details["flowers"] = ("A plain purple vase filled with flowers."
                                     "The flowers appear to have been freshly "
                                     "picked from the wildflowers that can be "
                                     "found around the town.")
pallet_home.db.details["wildflowers"] = ("A plain purple vase filled with "
                                         "flowers. The flowers appear to have "
                                         "been freshly picked from the "
                                         "wildflowers that can be found "
                                         "around the town.")
pallet_home.db.details["bookshelf"] = ("The bookshelf is crammed full of "
                                       "Pokemon books!")

pallet_home_mum = create_object(characters.BaseNPC, location=pallet_home,
                                key="Mum", aliases=["Mom", "mom", "mum"])
pallet_home_mum.db.desc = ("TO DO")
pallet_home_mum.db.conversation = "pallet_home"

home_ex1 = create_object(exits.Exit, key="Exit", aliases=["e", "exit"],
                         location=pallet_home,
                         destination=pallet_oshome)
home_ex2 = create_object(exits.QuantumExit, key="Up",
                         aliases=["u", "up", "upstairs"],
                         location=pallet_home)


"""
3. Pallet Town House
    1▒┌──┐▒▒▓Θ
    ▒▒╞══╡▒▒▓Θ
    ▒□└■─┘▒▒▓Θ
    ◄▒▒3▒▒▒▒▓Θ
    ◄▒▒▒▒▒▒▒▓Θ
    ┌────┐▒▒▓Θ

"""

pallet_oshouse.db.desc = "TO DO"

# pallet_oshouse.db.details[detailkey.lower()] = "description"

oshouse_ex1 = create_object(exits.Exit, key="West", aliases=["w", "west"],
                            location=pallet_oshouse,
                            destination=pallet_crossroads)
oshouse_ex2 = create_object(exits.Exit, key="Enter", aliases=["e", "enter"],
                            location=pallet_oshouse,
                            destination=pallet_house)

pallet_house.db.desc = "TO DO"

# pallet_house.db.details[detailkey.lower()] = "description"

pallet_house_girl = create_object(characters.BaseNPC, location=pallet_house,
                                 key="young girl", aliases=["girl"])
pallet_house_girl.db.desc = "TO DO"
pallet_house_girl.db.conversation = "pallet_house"

house_ex1 = create_object(exits.Exit, key="Exit", aliases=["e", "exit"],
                          location=pallet_house,
                          destination=pallet_oshouse)


"""
4. Pallet Town River Bank
    Θ▓▒▒πππ□▲▲╞
    Θ▓▒▒▓▓▓▓▒▒│
    Θ▓▒▒▓▓▓▓▒▒└
    Θ▓▒▒▒4▒▒▒▒►
    Θ▓▒▒▒▒▒▒▒▒π
    Θ▓▓▓≈≈≈≈▒▒▓

"""

pallet_riverbank.db.desc = ("The dense forest descending from the rolling "
                            "hills nearby to the south and west is cut short "
                            "by the path of a wide but calm river. Coming "
                            "from the north, a golden soil road follows along "
                            "the river for a short distance before turning "
                            "east from which direction a large building can "
                            "be seen, its large roof dwarfing the smaller "
                            "nearby houses to the north. In the intervening "
                            "space lay a clearing. Protected by a short "
                            "wooden fence, the clearing is marked with bursts "
                            "of colour given off by wildflowers flourishing "
                            "with the assistance of the nearby river. The "
                            "steady sound of running water fills the air.")

pallet_riverbank.db.details["forest"] = ("The rolling hills and mountains "
                                         "which surround the small town are "
                                         "forested with thick green trees. "
                                         "These untamed places look unsafe "
                                         "for anyone but the best pokemon "
                                         "trainers.")
pallet_riverbank.db.details["river"] = ("Calm and tranquil, the river flows "
                                        "steadily south. Occasionally ripples "
                                        "spread along it's surface. It might "
                                        "have been a Pokemon!")
pallet_riverbank.db.details["road"] = ("The compacted soil road appears to "
                                       "have it's surface routinely stamped "
                                       "all over by a large heavy Pokemon to "
                                       "keep it's surface firm.")
pallet_riverbank.db.details["fence"] = ("A sturdy unpainted wooden fence "
                                        "which divides the riverbank area "
                                        "from the road.")
pallet_riverbank.db.details["flowers"] = ("Bursts of yellows, reds and blues, "
                                          "the sweet scent of these flowers "
                                          "are as pleasing to the nose as "
                                          "their colours are on the eyes.")

pallet_riverbank_girl = create_object(characters.BaseNPC,
                                      location=pallet_riverbank,
                                      key="young girl", aliases=["girl"])
pallet_riverbank_girl.db.desc = ("TO DO")
pallet_riverbank_girl.conversation = "pallet_riverbank"

riverbank_ex1 = create_object(exits.Exit, key="North", aliases=["n", "north"],
                              location=pallet_riverbank,
                              destination=pallet_crossroads)
riverbank_ex2 = create_object(exits.Exit, key="East", aliases=["e", "east"],
                              location=pallet_riverbank,
                              destination=pallet_oslab)


"""
5. Oak's Laboratory
    ▒┌────┐▒▒▓Θ
    ▒╞════╡▒▒▓Θ
    ▒│    │▒▒▓Θ
    ▒└─■──┘▒▒▓Θ
    ◄▒▒5▒▒▒▒▒▓Θ
    ▒πππ□ππ▼▼▓Θ
"""
                
pallet_oslab.db.desc = ("The surrounding country side extends outwards from "
                        "the base of this hill, a landscape of thick forested "
                        "slopes stopping short only for the small village "
                        "resting quietly at it’s base. To the east, at the "
                        "bottom of a nearby staircase, a dirt road traces "
                        "it’s way into town and beside it, a river flows "
                        "steadily towards the south where it meets an even "
                        "greater body of water in the distance. Dominating "
                        "the landscape stands a large facility whose red "
                        "domed roof sits upon it’s tall yellow walls. Towards "
                        "the rear of the structure protrudes a large "
                        "windmill, it’s yellow blades emitting a low steady "
                        "whir. The doors to the facility appear open to "
                        "visitors with a sign posted close by. A small path "
                        "nearby leads south out of sight.")

pallet_oslab.db.details["forest"] = ("The rolling hills and mountains "
                                     "which surround the small town are "
                                     "forested with thick green trees. "
                                     "These untamed places look unsafe "
                                     "for anyone but the best pokemon "
                                     "trainers.")
pallet_oslab.db.details["road"] = ("The compacted soil road appears to "
                                   "have it's surface routinely stamped "
                                   "all over by a large heavy Pokemon to "
                                   "keep it's surface firm.")
pallet_oslab.db.details["stairs"] = ("A tall concrete set of stairs which "
                                     "descends ease down to the dirt road "
                                     "below.")
pallet_oslab.db.details["village"] = ("Extending from the base of the hill "
                                      "on which the facility stands, red "
                                      "tiled roofs stand intermittently along "
                                      "the path of a golden soiled road.")
pallet_oslab.db.details["windmill"] = ("Dwarfing the already large laboratory"
                                       ", a tall metallic pillar extends "
                                       "from the rear of the building and at "
                                       "it's top four yellow rotors turn "
                                       "briskly in the breeze.")

pallet_oslab_boy = create_object(characters.BaseNPC, location=pallet_oslab,
                                key="young boy", aliases=["boy"])
pallet_oslab_boy.db.desc = ("TO DO")
pallet_oslab_boy.db.conversation = "pallet_oslab"

oslab_ex1 = create_object(exits.Exit, key="West", aliases=["w", "west"],
                          location=pallet_oslab,
                          destination=pallet_riverbank)
oslab_ex2 = create_object(exits.Exit, key="South", aliases=["s", "south"],
                          location=pallet_oslab,
                          destination=pallet_park)
oslab_ex3 = create_object(exits.Exit, key="Enter", aliases=["e", "enter"],
                          location=pallet_oslab,
                          destination=pallet_lab)
                
pallet_lab.db.desc = ("The atrium of the laboratory consists of an open "
                      "rectangular space the rear border of which is formed "
                      "by a solid wall of bookshelves containing texts of "
                      "various sizes. Amongst the numerous piles of journals "
                      "and messy work spaces which inhabit this space, two "
                      "large pot plants breath life into what would otherwise "
                      "be a sterile environment. A gap in the book shelves "
                      "towards their centre of the rear wall creates an "
                      "entrance through which a number of scientific looking "
                      "machines can be seen deeper into the laboratory.")

pallet_lab.db.details["bookshelves"] = "TO DO"
pallet_lab.db.details["journals"] = "TO DO"
pallet_lab.db.details["plants"] = "TO DO"

pallet_lab_aide = create_object(characters.BaseNPC, location=pallet_lab,
                                key="Laboratory Aide", aliases=["aide"])
pallet_lab_aide.db.desc = ("TO DO")
pallet_lab_aide.db.conversation = "pallet_lab1"

pallet_lab_girl = create_object(characters.BaseNPC, location=pallet_lab,
                                key="young girl", aliases=["girl"])
pallet_lab_girl.db.desc = ("TO DO")
pallet_lab_girl.db.conversation = "pallet_lab2"

lab_ex1 = create_object(exits.Exit, key="Enter", aliases=["enter"],
                        location=pallet_lab,
                        destination=pallet_oak)
lab_ex2 = create_object(exits.Exit, key="Exit", aliases=["exit"],
                        location=pallet_lab,
                        destination=pallet_oslab)
                
pallet_oak.db.desc = ("A large room containing all manner of scientific "
                      "machinery of complicated function and design. Of these "
                      "devices, a computer stands out in the corner of the "
                      "room. Along the rear wall is a series of bookshelves "
                      "with posters hanging in the spaces in between. The"
                      " room itself appears immaculately maintained - "
                      "everything having a place. Out of place however "
                      "is a table standing in the centre of the room, "
                      "although at the moment there is nothing on it. A gap "
                      "in the furniture creates an entrance to the front of "
                      "the building whilst a glass door appears to lead "
                      "north to outside.")

pallet_oak.db.details["bookshelf"] = ("The books are encyclopedia-like, but "
                                      "the pages are blank!")
pallet_oak.db.details["bookshelves"] = ("The books are encyclopedia-like, but "
                                        "the pages are blank!")
pallet_oak.db.details["posters"] = "Type MENU to display the menu options!"
pallet_oak.db.details["table"] = ("The sturdy wooden table has three circular "
                                  "grooves where three circular objects might "
                                  "normally sit.")

oak_ex1 = create_object(exits.Exit, key="Enter", aliases=["enter"],
                        location=pallet_oak,
                        destination=pallet_corral)
oak_ex2 = create_object(exits.Exit, key="Exit", aliases=["exit"],
                        location=pallet_oak,
                        destination=pallet_lab)

pallet_corral.db.desc = ("Emerging from the rear of the laboratory, a wooden "
                         "fence encircles a large field dominated by the "
                         "tall windmill rising from the laboratory's "
                         "structure. The field itself is encircled by a thick "
                         "line of tree cover almost completely blocking it’s "
                         "view from the surrounding country side. The grass "
                         "within the field is green and lush creating a "
                         "carpet of soft ground. The Laboratories rear door, "
                         "to the south of the field, appears to be the only "
                         "way out.")

pallet_corral.db.details["laboratory"] = "TO DO"
pallet_corral.db.details["fence"] = ("A fence made of solid timber standing "
                                     "as tall as a man. The long planks from "
                                     "which it is built appear sturdy enough "
                                     "to contain all but the strongest "
                                     "Pokemon.")
pallet_corral.db.details["windmill"] = ("Dwarfing the already large laboratory"
                                        ", a tall metallic pillar extends "
                                        "from the rear of the building and at "
                                        "it's top four yellow rotors turn "
                                        "briskly in the breeze.")
pallet_corral.db.details["forest"] = ("The rolling hills and mountains "
                                      "which surround the small town are "
                                      "forested with thick green trees. "
                                      "These untamed places look unsafe "
                                      "for anyone but the best pokemon "
                                      "trainers.")

corral_ex1 = create_object(exits.Exit, key="Exit", aliases=["exit"],
                           location=pallet_corral,
                           destination=pallet_oak)

"""
7. Pallet Town River

    Θ▓▒▒▲▲▲▲▒▒π
    Θ▓▓▓≈≈≈≈▒▒▓
    Θ▓▓▓≈6≈≈▒▒▓
    Θ▓▓▓≈≈≈≈▒▒▒
    ΘΘ▓▓≈≈≈≈ΘΘΘ
        ▼▼▼▼
"""

# pallet_river.db.desc = "TO DO"
#
# pallet_river.db.details[detailkey.lower()] = "description"
#
# pallet_river_ex1 = create_object(exits.Exit, key="North", aliases=["n"],
#                             location=pallet_river,
#                             destination=pallet_riverbank)
# pallet_river_ex2 = create_object(exits.Exit, key="South", aliases=["s"],
#                             location=pallet_river,
#                             destination=)

"""
6. Pallet Town Park
    ▒▒▒πππ□ππ▲▲▓Θ
    ≈▒▒▓▓▓▓▓▓▒▒▓Θ
    ≈▒▒▓▓7▓▓▓▒▒▓Θ
    ≈▒▒▒▒▒▒▒▒▒▒▓Θ
    ≈ΘΘΘΘΘΘΘΘΘΘΘΘ
"""

pallet_park.db.desc = ("Under the shadow of the dominating laboratory to the "
                       "north, lay a small field extending only a short "
                       "distance before being greedily swallowed by the "
                       "encroaching forest to the south and east. Faintly "
                       "painted on the ground is a large white circle, the "
                       "ground inside of which appears to be sheer confusion. "
                       "Parts appear churned, others waterlogged and still "
                       "other parts charred by open flame. It appears to be a "
                       "location where Pokemon battles routinely take place. "
                       "A small path to the north leads towards the large "
                       "building.")

pallet_park.db.details["laboratory"] = "TO DO"
pallet_park.db.details["forest"] = ("The rolling hills and mountains "
                                    "which surround the small town are "
                                    "forested with thick green trees. "
                                    "These untamed places look unsafe "
                                    "for anyone but the best pokemon "
                                    "trainers.")
pallet_park.db.details["circle"] = ("The circle appears to be painted ground "
                                    "but is interrupted and broken by gouges "
                                    "and churned soil originating from "
                                    "within.")
              
park_ex1 = create_object(exits.Exit, key="North", aliases=["n", "north"],
                         location=pallet_park,
                         destination=pallet_oslab)
