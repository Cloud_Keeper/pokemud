"""

The itemlist holds all hardcoded values for items.

"""

ITEMLIST = {

    "Potion": {
        "category": "consumable",
        "price": 300,
        "desc": "A potion",
        "effect": "A function",
    },

    "Super Potion": {
        "category": "consumable",
        "price": 700,
        "desc": "A Super potion",
        "effect": "A function",
    },

    "Pokeball": {
        "category": "pokeball",
        "price": 200,
        "desc": "A pokeball",
        "effect": "A function",
        "rate": 255,
    }
}
