"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
import time
from evennia.utils import evtable
from evennia import create_object, search_object
from evennia.utils import lazy_property
from typeclasses import rooms, exits
from typeclasses.handler_equipment import EquipHandler
from evennia.contrib.rpsystem import ContribRPCharacter
from evennia.commands import cmdset, command


class Character(ContribRPCharacter):
    """
    Characters for Pokemud.
    """

    @lazy_property
    def equip(self):
        """Handler for equipped items."""
        return EquipHandler(self)

    def at_object_creation(self):
        """This is called when object is first created."""
        super(Character, self).at_object_creation()
        # Currently restricting SDESC functionality
        self.sdesc.add(self.key)
        self.db.pose_default = ""

        self.db.limbs = (
            ("head", ("head", "face", "neck")),
            ("body", ("shirt", "jacket", "backpack")),
            ("left_arm", ("left_arm", "left_hand")),
            ("right_arm", ("right_arm", "right_hand")),
            ("legs", ("legs", "left_foot", "right_foot")),
        )
        self.db.slots = {
            "head": None,
            "face": None,
            "neck": None,
            "shirt": None,
            "jacket": None,
            "backpack": None,
            "left_arm": None,
            "left_hand": None,
            "right_arm": None,
            "right_hand": None,
            "legs": None,
            "left_foot": None,
            "right_foot": None,
        }

    def at_before_move(self, destination):
        """
        Called just before starting to move this object to
        destination.

        Args:
            destination (Object): The object we are moving to

        Returns:
            shouldmove (bool): If we should move or not.

        Notes:
            If this method returns False/None, the move is cancelled
            before it is even started.

        """
        # Pose resets whenever moved.
        self.db.pose = self.db.pose_default
        return True

    def return_appearance(self, looker):
        """
        This formats a description. It is the hook a 'look' command
        should call.

        Args:
            looker (Object): Object doing the looking.
        """
        if not looker:
            return
        # Variable set up.
        name = self.get_display_name(looker)
        pose = self.db.pose
        desc = self.db.desc
        prefix = "You are" if looker is self else "They are"
        
        # Title of screen.
        string = "{c%s{n" % name
        # Current pose.
        string += "\n{} {}".format(name, pose) if pose else ""
        # Current description.
        string += "\n{}".format(desc)
        # Display visible equipped equipment.
        equipped = []
        for slot, item in self.equip:
            if not item or not item.access(looker, 'view'):
                continue
            if item not in equipped:
                equipped.append(item)

        if len(equipped) <= 0:
            string += "\n{} not wearing anything but underwear!".format(
                prefix)
        else:
            table = evtable.EvTable(border="header")
            for item in equipped:
                table.add_row("{C%s{n" % item.name, item.db.desc or "")
            string += "\n|w{} wearing:|n\n{}".format(prefix, table)
        return string


class BaseCharacter(Character):
    """
    Characters for Pokemud.
    """

    def at_object_creation(self):
        """Called only once, when object is first created"""
        super(BaseCharacter, self).at_object_creation()

        self.db.game_progress = 0
        self.db.money = 3000
        self.db.time_played = 0.0
        self.db.badges = [False, False, False, False,
                          False, False, False, False]
        self.db.room = self.personal_bedroom()

        self.db.party = []
        self.db.box = []

    def add_to_party(self, pokemon):
        """
        Add to party. Checks that it's a pokemon should already be done.
        """
        # Store in None.
        pokemon.move_to(None, quiet=True, to_none=True)

        if len(self.db.party) < 6:
            self.db.party.append(pokemon)
        else:
            self.db.box.append(pokemon)

    def at_pre_unpuppet(self):
        """Called just before the beginning of un-puppeting"""

        # Update Time Played.
        sessions = self.sessions.get()
        session = sessions[0] if sessions else None
        # Actual time - time connected.
        self.db.time_played += time.time() - session.conn_time

    def personal_bedroom(self):
        """Creates personal bedroom accessed from Pallet_Home"""

        room = create_object(rooms.Room, key=self.key + "'s Bedroom")
        room.db.desc = ("In this small square room, the walls are covered "
                        "with various posters of Pokemon. Below them, a whole "
                        "wall is taken up by a neatly made bed, the of which "
                        "sheets are folded smoothly by what appears to have "
                        "been adept hands. On the opposite wall stands a desk "
                        "with a basic desktop computer sitting on top. In the "
                        "centre of the room a TV and Super Nintendo game "
                        "console is sprawled lazily on the floor. A set of "
                        "stairs leads down to the living area below.")

        # Put Objects here -Computer -TV

        # Bedroom Exit Setup
        destination = None
        try:
            destination = search_object('Home')[0]
        except:
            destination = search_object('Limbo')[0]
        room_exit = create_object(exits.Exit, key="Down",
                                  aliases=["down", "stairs", "downstairs"],
                                  location=room,
                                  destination=destination)
        return room


class BaseNPC(Character):
    """Basic talking NPC to be used in conjunction with talk command."""

    def at_object_creation(self):
        """This is called when object is first created."""
        super(BaseNPC, self).at_object_creation()

        self.db.conversation = None


class PokemonCommand(command.Command):
    """
    This is a command that simply cause the caller to traverse
    the object it is attached to.
    """
    obj = None

    def func(self):
        """
        Default exit traverse if no syscommand is defined.
        """

        if "return" in self.args:
            self.obj.location = None

        self.caller.msg("Test")


class BasePokemon(Character):
    """Pokemon character (HP, ATT, DEF, SPEC, SPEED)"""

    pokemon_command = PokemonCommand
    priority = 101

    def at_object_creation(self):
        """This is called when object is first created."""
        super(BasePokemon, self).at_object_creation()

        self.locks.remove("call")
        self.locks.add("call:true()")

        self.db.health = 10
        self.db.level = None
        self.db.exp = None
        self.db.moves = None
        
        self.db.iv = None
        self.db.ev = None

        self.db.owner = None
        
        self.db.gender = None

    def create_exit_cmdset(self, exidbobj):
        """
        Helper function for creating an exit command set + command.
        The command of this cmdset has the same name as the Exit
        object and allows the exit to react when the player enter the
        exit's name, triggering the movement between rooms.
        Args:
            exidbobj (Object): The DefaultExit object to base the command on.
        """

        # create an exit command. We give the properties here,
        # to always trigger metaclass preparations
        cmd = self.pokemon_command(key=exidbobj.db_key.strip().lower(),
                                   aliases=exidbobj.aliases.all(),
                                   obj=exidbobj,
                                   locks="cmd:all()")
        # create a cmdset
        exit_cmdset = cmdset.CmdSet(None)
        exit_cmdset.key = 'PokemonCmdSet'
        exit_cmdset.priority = self.priority
        exit_cmdset.duplicates = True
        # add command to cmdset
        exit_cmdset.add(cmd)
        return exit_cmdset

    # Command hooks
    def basetype_setup(self):
        """
        Setup exit-security
        You should normally not need to overload this - if you do make
        sure you include all the functionality in this method.
        """
        super(BasePokemon, self).basetype_setup()

        # setting default locks (overload these in at_object_creation()
        self.locks.add(
            ";".join(["puppet:false()",  # would be weird to puppet an exit
                      "get:false()"]))  # no-one can pick up the exit

    def at_cmdset_get(self, **kwargs):
        """
        Called just before cmdsets on this object are requested by the
        command handler. If changes need to be done on the fly to the
        cmdset before passing them on to the cmdhandler, this is the
        place to do it. This is called also if the object currently
        has no cmdsets.
        Kwargs:
          force_init (bool): If `True`, force a re-build of the cmdset
            (for example to update aliases).
        """

        if "force_init" in kwargs or not self.cmdset.has_cmdset("ExitCmdSet",
                                                                must_be_default=True):
            # we are resetting, or no exit-cmdset was set. Create one dynamically.
            self.cmdset.add_default(self.create_exit_cmdset(self), permanent=False)

    def at_init(self):
        """
        This is called when this objects is re-loaded from cache. When
        that happens, we make sure to remove any old ExitCmdSet cmdset
        (this most commonly occurs when renaming an existing exit)
        """
        self.cmdset.remove_default()

    def succesful_catch(self):
        """
        Called after successful pokeball catch to determine changes to pokemon
        """
        pass

    def unsuccesful_catch(self):
        """
        Called after unsuccessful catch to determine changes to pokemon
        """
        pass

    def pre_choose(self):
        """
        Called on choose command to see if pokemon wants to come out.
        """
        return True

    def post_choose(self):
        """
        Called after choose command to see what pokemon does after coming out.
        """
        pass

    def pre_return(self):
        """
        Called after choose command to see what pokemon does after coming out.
        """
        return True
