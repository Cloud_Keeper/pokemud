"""
Object

The Object is the "naked" base class for things in the game world.

Note that the default Character, Room and Exit does not inherit from
this Object, but from their respective default implementations in the
evennia library. If you want to use this class as a parent to change
the other types, you can do so by adding this as a multiple
inheritance.

"""
from evennia.contrib.rpsystem import ContribRPObject


class Object(ContribRPObject):
    """
    Base typeclass for everything

    """
    def at_object_creation(self):
        """
        This is called when object is first created.
        Set up for poses on objects.

        """
        super(Object, self).at_object_creation()
        self.db.pose_default = ""

    def at_before_move(self, destination):
        """
        Called just before starting to move this object to
        destination.

        Args:
            destination (Object): The object we are moving to

        Returns:
            shouldmove (bool): If we should move or not.

        Notes:
            If this method returns False/None, the move is cancelled
            before it is even started.

        """
        # Pose resets whenever moved.
        self.db.pose = self.db.pose_default
        return True

    def return_appearance(self, looker):
        """
        This formats a description. It is the hook a 'look' command
        should call.

        Args:
            looker (Object): Object doing the looking.
        """
        if not looker:
            return
        # Get all contents of self allowable by lockstrings.
        visible = (con for con in self.contents if con != looker and
                   con.access(looker, "view"))
        exits, users, things = [], [], {}
        for con in visible:
            key = con.get_display_name(looker, pose=True)
            if con.destination:
                # List all exits individually
                exits.append(key)
            elif con.has_player:
                # List all players individually
                users.append(key)
            else:
                # Group all other things by same name and pose.
                if key not in things:
                    things[key] = {"key": key,
                                   "number": 1}
                else:
                    things[key]["number"] += 1

        # Build description string.
        # Display obj name by itself.
        string = "|c%s|n" % self.get_display_name(looker)
        # Display obj pose.
        pose = self.db.pose
        if pose:
            string += "\n%s %s" % (self.get_display_name(looker), pose)
        # Display object description
        desc = self.db.desc
        if desc:
            string += "\n%s" % desc
        # Display exits individually
        if exits:
            string += "\n|wExits:|n " + ", ".join(exits)
        # Display "You see" if requied.
        if users or things:
            string += "\n{wYou see:{n "
        # Display players individually
        if users:
            string += ", ".join(users)
        # Display things grouped together.
        if things:
            for thing in things:
                number = ""
                if not things[thing]["number"] == 1:
                    number = str(things[thing]["number"]) + "x "
                string += "\n" + number + thing
        return string


class InfinityItem(Object):
    """
    Typeclass for Items.
    Attributes:
        value (int): monetary value of the item in CC
    """
    item = None
    restricted = []

    def at_object_creation(self):
        super(InfinityItem, self).at_object_creation()
        # Set up variables.
        self.db.item = self.item
        self.db.restricted = self.restricted
        self.locks.add(";".join(("puppet:false()",
                                 "equip:false()",
                                 "view:all()",
                                 "get:all()",
                                 )))

    def at_before_move(self, destination):
        """
        Called just before starting to move this object to
        destination.

        Args:
            destination (Object): The object we are moving to

        Returns:
            shouldmove (bool): If we should move or not.

        """
        # Restrict someone who's picked this up from seeing or getting again.
        dbref = destination.dbref
        self.db.restricted.append(dbref)
        lockargs = ",".join(self.db.restricted)
        self.locks.add(";".join(
            ("view: not idlist(" + lockargs + ")",
             "get: not idlist(" + lockargs + ")",
             )))
        # Create item of same name with values from item list.
        return False


class Item(Object):
    """
    Typeclass for Items.
    Attributes:
        value (int): monetary value of the item in CC
    """
    value = 0

    def at_object_creation(self):
        super(Item, self).at_object_creation()
        # Set up variables.
        self.db.value = self.value
        self.locks.add(";".join(("puppet:false()",
                                 "equip:false()",
                                 )))

    def use_object(self, character, target):
        """
        To be called on use command.

        checks done by command.
        """
        return "You cannot use this object."


class Consumable(Item):
    """

    """

    def at_object_creation(self):
        super(Consumable, self).at_object_creation()
        self.locks.add("use:false()")

    def at_before_move(self, destination):
        """
        Called just before starting to move this object to
        destination.

        """
        # Only the holder can use.
        dbref = destination.dbref
        self.locks.add("use:id(" + dbref + ")")
        return True

    def use_object(self, caller, target):
        """
        To be called on use command.

        checks done by command.

        """
        key = self.key

        if key == "Potion":
            caller.msg("Used Potion.")
        if key == "Hyper Potion":
            caller.msg("Used Hyper Potion.")
        # ETC


class Pokeball(Consumable):
    """

    """

    def use_object(self, caller, target):
        """

        """

        #if not isinstance(target, characters.BasePokemon):
        #    return "Pokeball can only be used on Pokemon."

        #Rules return True / False

        target.db.owner = caller
        caller.add_to_party(target)

        caller.msg("You caught %s." % target.name)
        caller.location.msg_contents("%s caught %s." %
                                     (caller.name,
                                      target.name),
                                     exclude=caller)
        # calling hook method
        # pokemon.at_caught()


class Equippable(Item):
    """
    Typeclass for equippable Items.
    Attributes:
        slots (str, list[str]): list of slots for equipping
        multi_slot (bool): operator for multiple slots. False equips to
            first available slot; True requires all listed slots available.
    """
    slots = None
    multi_slot = False

    def at_object_creation(self):
        super(Equippable, self).at_object_creation()
        self.locks.add("puppet:false();equip:true()")
        self.db.slots = self.slots
        self.db.multi_slot = self.multi_slot
        self.db.used_by = None

    def use_object(self, character):
        """
        To be called on use command.

        checks done by command.
        """
        character.execute_cmd("equip " + self.key)
        return

    def at_equip(self, character):
        """
        Hook called when an object is equipped by character.
        Args:
            character: the character equipping this object
        """
        pass

    def at_remove(self, character):
        """
        Hook called when an object is removed from character equip.
        Args:
            character: the character removing this object
        """
        pass

    def at_drop(self, dropper):
        super(Equippable, self).at_drop(dropper)
        if self in dropper.equip:
            dropper.equip.remove(self)
            self.at_remove(dropper)