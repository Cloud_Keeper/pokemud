"""
Room

Rooms are simple containers that has no location of their own.

"""

from typeclasses import objects


class Room(objects.Object):
    """
    Rooms are like any Object, except their location is None
    (which is default). They also use basetype_setup() to
    add locks so they cannot be puppeted or picked up.
    (to change that, use at_object_creation instead)

    Extended to include detials
    """

    def at_object_creation(self):
        """Called when room is first created only."""
        super(Room, self).at_object_creation()
        # detail storage
        self.db.details = {}

    def return_detail(self, key):
        """
        This will attempt to match a "detail" to look for in the room.

        Args:
            key (str): A detail identifier.

        Returns:
            detail (str or None): A detail matching the given key.at_object_creation

        Notes:
            A detail is a way to offer more things to look at in a room
            without having to add new objects. For this to work, we
            require a custom `look` command that allows for `look
            <detail>` - the look command should defer to this method on
            the current location (if it exists) before giving up on
            finding the target.

            Details are not season-sensitive, but are parsed for timeslot
            markers.
        """
        try:
            detail = self.db.details.get(key.lower(), None)
        except AttributeError:
            # this happens if no attribute details is set at all
            return None
        if detail:
            return detail
        return None
