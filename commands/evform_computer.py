# encoding=utf-8
"""
This EvForm is used by the CmdTrainer in commands/charactercmds.CmdTrainer
It provides the player with a sort of character sheet following the aesthetic
of the Pokeon gameboy games.
"""

FORMCHAR = "x"

FORM = """
◓ xAxxxxxxxxxxxxxxxxxxxx :LxBx  xCx / xDx


                ◓═════════════════════════════════════════◓
                ║ 1. BILL's PC                            ║
                ║ 2. xAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 3. PROF. OAK's PC                       ║
                ║ 4. LOG OFF                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║          RED turned on the PC           ║
                ║                                         ║
                ◓═════════════════════════════════════════◓


                ◓═════════════════════════════════════════◓
                ║ 1. WITHDRAW PKMN                        ║
                ║ 2. DEPOSIT  PKMN                        ║
                ║ 3. RELEASE  PKMN                        ║
                ║ 4. SEE YA!                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║           Accessed Bill's PC.           ║
                ║                                         ║
                ◓═════════════════════════════════════════◓


                ◓═════════════════════════════════════════◓
                ║ 1. BILL's PC                            ║
                ║ 2. xAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 3. PROF. OAK's PC                       ║
                ║ 4. LOG OFF                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║                                         ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                
                
                ◓═════════════════════════════════════════◓
                ║ 1. WITHDRAW PKMN                        ║
                ║ 2. DEPOSIT  PKMN                        ║
                ║ 3. RELEASE  PKMN                        ║
                ║ 4. SEE YA!                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║    Accessed POKEeMON Storage System.    ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                
                
                ◓═════ ◓══════════════════════════════════◓
                ║ 1. W ║ 1. xAxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 2. D ║ 2. xBxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 3. R ║ 3. xCxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 4. S ║ 4. xDxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║      ║ 5. xExxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ◓═════ ║ 6. xFxxxxxxxxxxxxxxxx ◓══════════◓
                ║    A ║ 7. xGxxxxxxxxxxxxxxxx ║ WITHDRAW ║       
                ║      ║ 8. xHxxxxxxxxxxxxxxxx ║ STATS    ║
                ║      ║ 9. xIxxxxxxxxxxxxxxxx ║ CANCEL   ║
                ◓═════ ◓═══════════════════════◓══════════◓
                
                
                ◓═════════════════════════════════════════◓
                ║ 1. WITHDRAW PKMN                        ║
                ║ 2. DEPOSIT  PKMN                        ║
                ║ 3. RELEASE  PKMN                        ║
                ║ 4. SEE YA!                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║      xxxxxxxxx was stored in Box.       ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                
                ◓═════════════════════════════════════════◓
                ║ 1. WITHDRAW ITEM                        ║
                ║ 2. DEPOSIT  ITEM                        ║
                ║ 3. TOSS     ITEM                        ║
                ║ 4. LOG OFF                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║      xxxxxxxxx was stored in Box.       ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                
                ◓═════ ◓══════════════════════════════════◓
                ║ 1. W ║ 1. xAxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 2. D ║ 2. xBxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 3. R ║ 3. xCxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 4. S ║ 4. xDxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║      ║ 5. xExxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ◓═════ ║ 6. xFxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║    A ║ 7. xGxxxxxxxxxxxxxxxx ◓══════════◓       
                ║      ║ 8. xHxxxxxxxxxxxxxxxx ║ WITHDRAW ║
                ║      ║ 9. xIxxxxxxxxxxxxxxxx ║ CANCEL   ║
                ◓═════ ◓═══════════════════════◓══════════◓
                
                ◓═════════════════════════════════════════◓
                ║ 1. WITHDRAW ITEM                        ║
                ║ 2. DEPOSIT  ITEM                        ║
                ║ 3. TOSS     ITEM                        ║
                ║ 4. LOG OFF                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║             Withdrew POTION             ║
                ║                                         ║
                ◓═════════════════════════════════════════◓

                ◓═════════════════════════════════════════◓
                ║ POKeDEX completion is:                  ║
                ║                                         ║
                ║            145 POKeMON seen             ║
                ║            152 POKeMON owned            ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║     Accessed POKeDEX Rating System.     ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                
                ◓═════════════════════════════════════════◓
                ║ PROF. OAK's Rating:                     ║
                ║                                         ║
                ║  You finally got at least 50 species!   ║
                ║  Be sure to get Exp.All from my Aide!   ║
                ║                                         ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║                                         ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                
                Accessed my PC.

                Accessed Item Storage System.

                Accessed PROF. OAK's PC.

                Accessed POKeDEX Rating System.

                Want to get your POKeDEX rated? YES/NO

                POKeDEX completion is:
                145 POKeMON seen
                152 POKeMON owned

                PROF. OAK's Rating:
"""