
# ---------- COMMANDS ----------

from evennia.utils.utils import class_from_module, to_str
from django.conf import settings
from ast import literal_eval
from utils.create_pokemon import create_pokemon
from evennia import CmdSet
from evennia import utils

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)


class BuildCmdSet(CmdSet):
    """CmdSet for ... commands."""
    key = "charactercmdset"
    priority = 1

    def at_cmdset_creation(self):
        # Room Building
        self.add(CmdDetail())
        # Pokemon Building
        self.add(CmdPokespawn())


class CmdDetail(COMMAND_DEFAULT_CLASS):
    """
    `@detail` - allows to assign a "detail" (a non-object target for the
    `look` command) to the current room (only).

    Usage:
      @detail[/del] [<key> = <description>]

    Switch for `@detail`:
      del   - delete a named detail.

    This is identical to the details implementation in the Extended_Room
    contrib, however as we are only utilising part of the contribs func
    it is included in isolation rather than referencing the contrib.
    """
    key = "@detail"
    aliases = []
    locks = "perm(Builders)"
    help_category = "Building"

    def func(self):
        "Define extended command"

        caller = self.caller
        location = caller.location

        if not location:
            caller.msg("No location to detail!")
            return
        if location.db.details is None:
            caller.msg("|rThis location does not support details.|n")
            return
        if self.switches and self.switches[0] in 'del':
            # removing a detail.
            if self.lhs in location.db.details:
                del location.db.details[self.lhs]
            caller.msg("Detail %s deleted, if it existed." % self.lhs)
            return
        if not self.args:
            # No args given. Return all details on location
            string = "|wDetails on %s|n:" % location
            details = "\n".join(" |w%s|n: %s" % (key, utils.crop(text)) for key, text in location.db.details.items())
            caller.msg("%s\n%s" % (string, details) if details else "%s None." % string)
            return
        if not self.rhs:
            # no '=' used - list content of given detail
            if self.args in location.db.details:
                string = "{wDetail '%s' on %s:\n{n" % (self.args, location)
                string += str(location.db.details[self.args])
                caller.msg(string)
            else:
                caller.msg("Detail '%s' not found." % self.args)
            return
        # setting a detail
        location.db.details[self.lhs] = self.rhs
        caller.msg("Set Detail %s to '%s'." % (self.lhs, self.rhs))
        return


class CmdPokespawn(COMMAND_DEFAULT_CLASS):
    """
    Create a Pokemon. Fairly forgiving. As long as Pokemon name is right
    you'll get a pokemon on the other side.
    """

    key = "@pokespawn"
    aliases = ["@ps"]
    locks = "cmd:perm(spawn) or perm(Builders)"
    help_category = "Building"

    def func(self):
        
        # If no arguments provided, provide instructions.
        if not self.args:
            self.caller.msg("Usage: @pokespawn Pokemon OR {'key':'value', ..}")
            return
        
        # Convert arguments to a Python Construct or a string.
        try:
            args = literal_eval(self.args)
        except (SyntaxError, ValueError):
            args = to_str(self.args)
         
        # If string: create pokemon from just string.
        if isinstance(args, basestring):
            poke = create_pokemon(args, location=self.caller.location)
            
        # If not string or dict then abort.
        elif not isinstance(args, dict):
            self.caller.msg("Usage: @pokespawn Pokemon OR {'key':'value', ..}")
            return
            
        else:
            # Left with dictionary. Create pokemon with values given.
            
            default_values = {"key": None,
                              "location": self.caller.location,
                              "level": 1,
                              "iv": None,
                              "ev": None,
                              "owner": None}
            
            # Replace defaults with given values.
            for key in args:
                if key in default_values:
                    default_values[key] = args[key]
                else:
                    # If no match, inform user and proceed without.
                    string = " not valid argument. Proceedings without."
                    self.caller.msg(str(key) + ":" + str(args[key]) + string)
            
            # Create pokemon from dictionary
            poke = create_pokemon(default_values["key"],
                                  location = default_values["location"],
                                  level = default_values["level"],
                                  iv = default_values["iv"],
                                  ev = default_values["ev"],
                                  owner = default_values["owner"])
                                  
        if poke:
            self.caller.msg(str(poke.key) + " " + str(poke.dbref) + " spawned.")
        else: 
            #SHOULD RETURN ERROR
            self.caller.msg("Command failed.")
