# encoding=utf-8
"""
This EvForm is used by the CmdTrainer in commands/charactercmds.CmdTrainer
It provides the player with a sort of character sheet following the aesthetic
of the Pokeon gameboy games.
"""

FORMCHAR = "x"

FORM = """
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃  No. xAx      xxxxxBxxxxxxxxxxxxxxxxxxx  ┃
                ┃               xxxxxCxxxxxxxxxxxxxxxxxxx  ┃
                ┃               HT xxDxxxxxxxxxxxxxxxxxxx  ┃
                ┃               WT xxExxxxxxxxxxxxxxxxxxx  ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃  xxxxxxxxxxxxxxxxxxFxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛\
"""


