# encoding=utf-8
"""
This EvForm is used by the CmdTrainer in commands/charactercmds.CmdTrainer
It provides the player with a sort of character sheet following the aesthetic
of the Pokeon gameboy games.
"""

FORMCHAR = "x"

FORM = """

                  x1xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x3xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x4xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x6xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                 ◓═════════════════════════════════════════◓
                 ║                                         ║
                 ║            Choose a POKEMON             ║
                 ║                                         ║
                 ◓═════════════════════════════════════════◓
"""


