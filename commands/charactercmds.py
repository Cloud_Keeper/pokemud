# -*- coding: utf-8 -*-

"""
PokeMud Commands.

These are the unique or altered commands used by PokeMud.

"""

from evennia.utils.evmenu import EvMenu, null_node_formatter
from typeclasses.objects import Equippable
from evennia import CmdSet
from evennia import default_cmds
from evennia.utils import utils, evform, evtable
from django.conf import settings
from world.rules import BADGES
from utils import poketext
import time

# Used by commands to inherit from the default Command Class in settings.
COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)

# Used by CmdExtendedLook
_AT_SEARCH_RESULT = utils.variable_from_module(*settings.SEARCH_AT_RESULT.rsplit('.', 1))

# Used by CmdInventory
_INVENTORY_ERRMSG = "You don't have '{}' in your inventory."

# Used by CmdUse
_USE_ERRMSG = "You don't have '{}' in your inventory."

# Used by CmdUse
_TGT_ERRMSG = "'{}' could not be located."

# Used by CmdEquip
_EQUIP_ERRMSG = "You do not have '{}' equipped."


class CharacterCmdSet(CmdSet):
    """
    The `CharacterCmdSet` contains additional, unique or altered commands used
    by PokeMud available on in-game Character objects. The commands are listed
    in correct order as they appear in the file. It is included in:
    `commands/default_cmdset.CharacterCmdSet.at_cmdset_creation`
    """
    key = "charactercmdset"
    priority = 1

    def at_cmdset_creation(self):
        # General Commands
        self.add(CmdExtendedLook())
        # Menu Commands
        self.add(CmdTrainer())
        self.add(CmdPokedex())
        self.add(CmdParty())
        # Inventory Commands
        self.add(CmdInventory())
        self.add(CmdUse())
        self.add(CmdEquip())
        self.add(CmdRemove())
        # NPC related Commands
        self.add(CmdNPCTalk())
        # Pokemon Commands
        self.add(CmdChoose())
        self.add(CmdReturn())

# GENERAL COMMANDS


class CmdExtendedLook(default_cmds.CmdLook):
    """
    look

    Usage:
      look
      look <obj>
      look <room detail>
      look *<player>

    Observes your location, objects or details at your location.
    This is the unaltered extended look command from the extended_room contrib.
    We are only using a limited amount of the extended_room functionality
    (just details) so at this stage we are including the relevant code
    ourselves instead of using the contrib as a whole.
    """
    def func(self):
        """
        Handle the looking - add fallback to details.
        """
        caller = self.caller
        args = self.args
        if args:
            looking_at_obj = caller.search(args,
                                           candidates=caller.location.contents + caller.contents,
                                           use_nicks=True,
                                           quiet=True)
            if not looking_at_obj:
                # no object found. Check if there is a matching
                # detail at location.
                location = caller.location
                if location and hasattr(location, "return_detail") and callable(location.return_detail):
                    detail = location.return_detail(args)
                    if detail:
                        # we found a detail instead. Show that.
                        caller.msg(detail)
                        return
                # no detail found. Trigger delayed error messages
                _AT_SEARCH_RESULT(looking_at_obj, caller, args, quiet=False)
                return
            else:
                # we need to extract the match manually.
                looking_at_obj = utils.make_iter(looking_at_obj)[0]
        else:
            looking_at_obj = caller.location
            if not looking_at_obj:
                caller.msg("You have no location to look at!")
                return

        if not hasattr(looking_at_obj, 'return_appearance'):
            # this is likely due to us having a player instead
            looking_at_obj = looking_at_obj.character
        if not looking_at_obj.access(caller, "view"):
            caller.msg("Could not find '%s'." % args)
            return
        # get object's appearance
        caller.msg(looking_at_obj.return_appearance(caller))
        # the object's at_desc() method.
        looking_at_obj.at_desc(looker=caller)

# MENU COMMANDS


class CmdTrainer(default_cmds.MuxCommand):
    """
    Trainer menu

    Usage:
      trainer

    This provides the player with their own character sheet, mirroring the form
    of the character sheet in the Pokemon game, displaying the players
    characters name, money, time played and badges.

    This utilises EvForm and the commands/evform_trainer file:
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃   NAME/   xxxxxAxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃   MONEY/  ₱xxxxBxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃   TIME/   xxxxxCxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛
                                   ●BADGES●
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃   1.        2.        3.         4.      ┃
                ┃       xDx       xEx       xFx        xGx ┃
                ┃   5.        6.        7.         8.      ┃
                ┃       xHx       xIx       xJx        xKx ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛\

    ** Consider extending so that "trainer <character>" provides the sheet to
    other players.
    ** Consider changing to EvTable. The Form doesn't seem to play to well with
    UTF-8 characters.
    """

    key = "trainer"
    locks = "cmd:all()"
    aliases = ["c", "cha", "character"]
    help_category = "General"

    def func(self):
        # Set up command variables. Required for A-C
        caller = self.caller
        # Connection durations are stored on session. Required for C
        sessions = self.caller.sessions.get()
        session = sessions[0] if sessions else None
        # Required for D-K
        gyms = caller.db.badges
        # Build EvForm from values.
        form = evform.EvForm("commands.evform_trainer")
        form.map(cells={"A": caller.key,
                        "B": caller.db.money,
                        "C": utils.time_format(caller.db.time_played +
                                               (time.time() -
                                                session.conn_time), 2),
                        "D": BADGES[0] if gyms[0] else "?",
                        "E": BADGES[1] if gyms[1] else "?",
                        "F": BADGES[2] if gyms[2] else "?",
                        "G": BADGES[3] if gyms[3] else "?",
                        "H": BADGES[4] if gyms[4] else "?",
                        "I": BADGES[5] if gyms[5] else "?",
                        "J": BADGES[6] if gyms[6] else "?",
                        "K": BADGES[7] if gyms[7] else "?"})
        self.msg(unicode(form))


class CmdPokedex(default_cmds.MuxCommand):
    """

    """

    key = "pokedex"
    locks = "cmd:all()"
    aliases = ["p"]
    help_category = "General"

    def func(self):
        # Set up command variables. Required for A-C
        caller = self.caller
        # Connection durations are stored on session. Required for C
        sessions = self.caller.sessions.get()
        session = sessions[0] if sessions else None
        # Required for D-K
        gyms = caller.db.badges
        # Build EvForm from values.
        form = evform.EvForm("commands.evform_pokedex")
        form.map(cells={"A": "001",
                        "B": "Bulbasaur",
                        "C": "Grass",
                        "D": '?" ??"',
                        "E": "??kg",
                        "F": ""})

        # form.map(cells={"A": "001",
        #                 "B": "Bulbasaur",
        #                 "C": "Grass",
        #                 "D": '5" 00"',
        #                 "E": "10kg",
        #                 "F": "Jigglypuff’s vocal cords can freely adjust the wavelength of its voice. This Pokémon uses this ability to sing at precisely the right wavelength to make its foes most drowsy."})
        self.msg(unicode(form))


class CmdParty(default_cmds.MuxCommand):
    """
    Party menu

    Usage:
      pokemon
      party

    This provides the player with their party screen, mirroring the form
    of the party screen in the Pokemon game, displaying the party members
     name, level, and health.

    This utilises EvForm and the commands/evform_party file:

                 ◓ PIDGEY                  :L99    0 /  26
                 ◓ CHARIZARD               :L99  270 / 270
                 ◓ MEW                     :L99   95 /  95
                 ◓ BLASTOISE               :L99  125 / 149
                 ◓ VENUSAUR                :L99  101 / 148
                 ◓ MEWTO                   :L99  166 / 166
                ◓═════════════════════════════════════════◓
                ║                                         ║
                ║            Choose a POKEMON             ║
                ║                                         ║
                ◓═════════════════════════════════════════◓

    ** Consider changing to EvTable. The Form doesn't seem to play to well with
    UTF-8 characters.
    """

    key = "party"
    locks = "cmd:all()"
    aliases = ["pokemon"]
    help_category = "General"

    def func(self):

        caller = self.caller
        party = caller.db.party

        cells = {}
        for pokemon in party:
            form = evform.EvForm("commands.evform_partyline")
            form.map(cells={
                "A": pokemon.key,
                "B": "   "[:-len(str(pokemon.db.level))] + str(pokemon.db.level),
                "C": "   "[:-len(str(pokemon.db.health))] + str(pokemon.db.health),
                "D": "   "[:-len(str(99))] + str(99)})
            cells[party.index(pokemon)+1] = form


        # Build EvForm from values.
        form = evform.EvForm("commands.evform_partyscreen")
        form.map(cells=cells)
        self.msg(unicode(form))

# INVENTORY COMMANDS


class CmdInventory(COMMAND_DEFAULT_CLASS):
    """
    view inventory

    Usage:
      inventory
      inv
      i

    This is an altered version of the Evennia stock inventory command which
    shows your inventory but excludes equipped items. The inventory list also
    condenses multiple instances of the same item into numbered listings:

        You are carrying:
        1 iron sword     A shiny medium sized blade.
        4 red delicious  A bright red Apple.

    ** Can simplify code by only running through list once.
    """
    key = "inventory"
    aliases = ["inv", "i"]
    locks = "cmd:all()"
    arg_regex = r"$"

    def func(self):
        """check inventory"""
        # Get Inventory
        player_contents = self.caller.contents
        # Remove equipped items.
        for slot, item in self.caller.equip:
            if item in player_contents:
                player_contents.remove(item)
        # Prepare inventory list.
        # Reducing list to name and number of item for display: {item, number}
        inventory = {}
        for item in player_contents:
            key = item.name
            if key not in inventory:
                inventory[key] = {"item": item,
                                  "number": 1}
            else:
                inventory[key]["number"] += 1

        # Return appropriate inventory message
        if not inventory:
            string = "You are not carrying anything."
        else:
            table = evtable.EvTable(border="header")
            for item in inventory:
                # Each column separated by a comma. add_row(col1,col2,col3).
                table.add_row("{C%s{n" % str(inventory[item]["number"]),
                              item,
                              inventory[item]["item"].db.desc or "")
            string = "{wYou are carrying:\n%s" % table
        self.caller.msg(string)


class CmdUse(COMMAND_DEFAULT_CLASS):
    """
    Use an object

    Usage:
        use <obj> [= <target>]

    The command takes inspiration by the use command and hooks in the Muddery
    Evennia project. The command is used on items in the characters inventory.
    The command simply sanity checks arguments before calling the objects
    use_object() function. A string is returned by the function which is then
    passed on to the character.

    ** at this stage only in your inventory.
    """
    key = "use"
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Use an object."""
        # Set up function variables
        caller = self.caller
        obj = self.lhs
        target = self.rhs

        # If no item, prompt user.
        if not obj:
            caller.msg("Use what?")
            return

        # Search for object, giving error if not found.
        obj = caller.search(obj, candidates=caller.contents,
                            nofound_string=_USE_ERRMSG.format(obj))

        # End command if no matching object found.
        if not obj:
            # Could return list of all usable objects in inventory?
            return

        # If target given, find target.
        if target:
            target = caller.search(target,
                                   nofound_string=_TGT_ERRMSG.format(target))

        # Call use_object hook on object.
        try:
            # Call obj.use_object hook.
            obj.use_object(caller, target)
            # Code I'm copying uses a hook on the character. I'm not sure
            # I need it but it's something to think about in future.
        except Exception:
            caller.msg("You cannot use this object.")


class CmdEquip(default_cmds.MuxCommand):
    """
    equip equippable items.

    Usage:
      equip[/swap] [<item>]
      wear[/swap] [<item>]
    Switches:
      s[wap] - replaces any currently equipped item

    Equips an item to its required slot(s). If no item specified, lists
    possible equippable items in your inventory. This is heavily modified
    versions of the Ainneve equipment commands. That has a master equip
    command and seperate commands for possible alternatives such as "wear"
    "wield" which merely call the equip command with some additional attributes
    This equipment command achieves the same thing but by using aliases and
    the cmd attribute (which shows the cmd/alias actually used) and a
    dictionary of corresponding typeclasses to have the same functionality.

    This is a dictionary which contains a list of all keys and aliases of
    this command and the typeclass that corresponds to them. This allows
    items to be equipped only by certain aliases "wear hat" or "wield axe".

    This is easily exapandable
    """
    key = "equip"
    aliases = ["eq", "wear", "wield"]
    locks = "cmd:all()"
    keymatrix = {
        ("equip", "eq"): ("equip", [Equippable]),
        ("wear"): ("wear", [Equippable]),
        ("wield"): ("wield", [Equippable]),
    }

    def _display_item_list(self, caller, typeclasses, cmd):
        """
        Returns a list of items in caller's inventory that can be equipped by
        the cmd specified.
        """
        # Create list of inventory items of right typeclass and not equiped.
        available = [i for i in caller.contents
                     if any(isinstance(i, typeclass) for typeclass in typeclasses)
                     and i not in caller.equip]
        # Message caller with table of options or message that nothing matches.
        if len(available) <= 0:
            output = "You have nothing to {} in your inventory".format(cmd)
        else:
            table = evtable.EvTable(border="header")
            for item in available:
                table.add_row("{C%s{n" % item.name, item.db.desc or "")
                output = "|wYou can {}:|n\n{}".format(cmd, table)
        caller.msg(output)
        return

    def func(self):
        """
        equips item
        """

        # Set up function variables.
        caller = self.caller
        cmd = self.cmdstring
        typeclasses = []
        swap = any(strings.startswith('s') for strings in self.switches)
        args = self.args.strip()

        # Convert cmd into appropriate action word to be used in player msgs.
        # I.e. cmd = "eq" -> cmd = "equip"
        for key in self.keymatrix:
            if cmd in key:
                cmd = self.keymatrix[key][0]
                typeclasses = self.keymatrix[key][1]

        # If no arguments, display all items that correspond to that command.
        if not args:
            caller.msg("{} what?".format(cmd.capitalize()))
            self._display_item_list(caller, typeclasses, cmd)
            return

        # Handle if there are arguments.
        # Search for target object, giving error message if it cannot be found.
        obj = caller.search(args,
                            candidates=caller.contents,
                            nofound_string=_INVENTORY_ERRMSG.format(
                                args))
        if not obj:
            # If no object found, display available items after err msg.
            self._display_item_list(caller, typeclasses, cmd)
            return

        # Check obj is of allowed typeclass for command used.
        matches = (isinstance(obj, typeclass) for typeclass in typeclasses)
        if not any(matches):
            caller.msg("You can't {} {}.".format(cmd, obj.name))
            return

        # Check if obj has correct permission.
        if not obj.access(caller, 'equip'):
            caller.msg("You can't {} {}.".format(cmd, obj.name))
            return

        # Check if target is already equipped.
        if obj in caller.equip:
            caller.msg(
                "You're already {}ing {}.".format(cmd, obj.name))
            return

        # Check whether slots required are occupied
        # Creates list of slots required that are occupied.
        occupied_slots = [caller.equip.get(item) for item in
                          obj.db.slots
                          if caller.equip.get(item)]

        # Code for multi_slot items. If not all slots available.
        if obj.db.multi_slot:
            if len(occupied_slots) > 0:
                # If swap switch, remove items in all req slots.
                if swap:
                    for item in occupied_slots:
                        caller.equip.remove(item)
                # If no switch abort command.
                else:
                    caller.msg("You can't {} {}. ".format(cmd, obj.name) +
                               "You already have something there.")
                    return

        # Code for single slot.
        else:
            if len(occupied_slots) == len(obj.db.slots):
                # If swap switch, remove item.
                if swap:
                    caller.equip.remove(occupied_slots[0])
                # If no swap switch, abort command.
                else:
                    caller.msg("You can't {} {}. ".format(cmd, obj.name) +
                               "You have no open {} slot{}.".format(
                                   ", or ".join(obj.db.slots),
                                   "s" if len(obj.db.slots) != 1 else ""
                               ))
                    return

        # This is where the work occurs. Because the add command
        # returns True or False on success it is wrapped in an if
        # supplying a message if it fails.
        if not caller.equip.add(obj):
            caller.msg("You can't {} {}.".format(cmd, obj.name))
            return

        # call hook
        if hasattr(obj, "at_equip"):
            obj.at_equip(caller)

        caller.msg("You {} {}.".format(cmd, obj))
        caller.location.msg_contents(
            "{} {}s {}.".format(caller.name.capitalize(), cmd,
                                obj.name), exclude=caller)

class CmdRemove(default_cmds.MuxCommand):
    """
    remove item
    Usage:
      remove <obj>
    Remove an equipped object and return it to your inventory. Ths is identical
    to the Ainneve Equipment implementation.
    """
    key = "remove"
    aliases = ["rem"]
    locks = "cmd:all()"

    def func(self):
        caller = self.caller
        args = self.args.strip()

        if not args:
            caller.msg("Remove what?")
            return

        # search for target in our equip
        equipped_items = [i[1] for i in caller.equip if i[1]]
        obj = caller.search(
            args,
            candidates=equipped_items,
            nofound_string=_EQUIP_ERRMSG.format(args))

        if not obj:
            return

        if not caller.equip.remove(obj):
            return

        # call hook
        if hasattr(obj, "at_remove"):
            obj.at_remove(caller)

        caller.msg("You remove {}.".format(obj.name))
        caller.location.msg_contents(
            "{} removes {}.".format(caller.name.capitalize(),
                                    obj.name),
            exclude=caller)

# NPC RELATED COMMANDS


class CmdNPCTalk(COMMAND_DEFAULT_CLASS):
    """
    TO BE REWORKED.
    """

    key = "talk"
    aliases = ["talk to"]
    locks = "cmd:all()"

    def func(self):

        if not self.args:
            string = "Talk to whom?"
            self.caller.msg(poketext.nobordertext(string, "Oak"))
            return
        else:
            target = self.caller.search(self.args)
            if not target:
                string = "Talk to whom?"
                self.caller.msg(poketext.nobordertext(string, "Oak"))
                return

        # If no conversation attribute return "You can't talk to that"
        if not target.attributes.has("conversation"):
            self.caller.msg(poketext.nobordertext("You can't talk to that.",
                                                  "Oak"))
            return

        npcname = target.key
        npcconversation = target.db.conversation

        # If conversation exists but is none return "..."
        if not npcconversation:
            npcconversation = "..."
            self.msg(poketext.nobordertext(npcconversation, npcname))
            return

        EvMenu(self.caller, "world.conversation",
               startnode=target.db.conversation,
               cmdset_mergetype="Replace", cmdset_priority=1,
               cmd_on_exit=None, node_formatter=null_node_formatter)

# POKEMON COMMANDS


class CmdChoose(default_cmds.MuxCommand):
    """
    Test Command.

    """

    key = "choose"
    aliases = ["go"]
    locks = "cmd:all()"
    help_category = "General"

    def func(self):

        caller = self.caller
        party = caller.db.party
        args = self.args

        #This should call a hook.
        # for pokemon in party:
            # if args == pokemon.key or party.index("pokemon"):
                # pokemon.location = caller.location
                # return
                
        for i in xrange(len(party)):
            if args == str(i+1) or args == party[i].key:
                party[i].location = caller.location
                return

        self.msg("No Pokemon by that name")

        
class CmdReturn(default_cmds.MuxCommand):
    """
    Test Command.

    """

    key = "return"
    aliases = ["recall"]
    locks = "cmd:all()"
    help_category = "General"

    def func(self):

        caller = self.caller
        party = caller.db.party
        args = self.args
        
        if not args:
            caller.msg("Return who?")
            return
        
        if args in ["all", "everyone"]:
            for pokemon in party:
                if pokemon.location == caller.location:
                    pokemon.location = None
                    caller.msg("Pokemon returned.")
            return
                
        for pokemon in party:
            if (args == pokemon.key or args in pokemon.aliases.all()) and pokemon.location == caller.location:
                pokemon.location = None
                caller.msg("Pokemon returned.")
                return
            
        caller.msg("No Pokemon by that name")
