# -*- coding: utf-8 -*-
"""
Commands that are available from the connect screen.
"""

import re
from collections import defaultdict
from django.conf import settings
from typeclasses import characters
from evennia import CmdSet
from evennia.commands.default import unloggedin
from evennia.players.models import PlayerDB
from evennia.objects.models import ObjectDB
from evennia.server.models import ServerConfig
from evennia.utils import logger, utils, ansi
from evennia.utils.evmenu import EvMenu, null_node_formatter
from utils import poketext
from evennia.commands.default.muxcommand import MuxCommand
from evennia.commands.cmdhandler import CMD_LOGINSTART
from evennia.commands.default.unloggedin import (create_guest_player,
                                                 _throttle,
                                                 create_normal_player,
                                                 _create_player,
                                                 _create_character)

# Used in login.
MULTISESSION_MODE = settings.MULTISESSION_MODE
# Used to display the unlogged in screen.
CONNECTION_SCREEN_MODULE = settings.CONNECTION_SCREEN_MODULE

# Helper function to throttle failed connection attempts.
_LATEST_FAILED_LOGINS = defaultdict(list)

class UnloggedinCmdSet(CmdSet):
    """
    The `CharacterCmdSet` contains additional, unique or altered commands used
    by PokeMud available at the unloggedin screen. The commands are listed
    in correct order as they appear in the file. It is included in:
    `commands/default_cmdset.CharacterCmdSet.at_cmdset_creation`
    """
    key = "charactercmdset"
    priority = 1

    def at_cmdset_creation(self):
        # Remove defaults. Will change settings during final polishing.
        self.remove(unloggedin.CmdUnconnectedConnect())
        self.remove(unloggedin.CmdUnconnectedCreate())
        self.remove(unloggedin.CmdUnconnectedQuit())
        self.remove(unloggedin.CmdUnconnectedHelp())
        self.remove(unloggedin.CmdUnconnectedLook())
        # Commands for unloggedin screen.
        self.add(CmdUnconnectedContinue())
        self.add(CmdUnconnectedNewGame())
        self.add(CmdUnconnectedQuit())
        self.add(CmdUnconnectedHelp())
        self.add(CmdUnconnectedLook())


class CmdUnconnectedContinue(MuxCommand):
    """
    Default connect command.

    Usage (at login screen):
      continue playername password
      continue "player name" "pass word"

    Use the create command to first create an account before logging in.

    If you have spaces in your name, enclose it in quotes.
    """
    key = "continue"
    aliases = ["cont", "con", "c", "connect", "conn"]
    locks = "cmd:all()"  # not really needed
    arg_regex = r"\s.*?|$"

    def func(self):
        """
        Uses the Django admin api. Note that unlogged-in commands
        have a unique position in that their func() receives
        a session object instead of a source_object like all
        other types of logged-in commands (this is because
        there is no object yet before the player has logged in)
        """
        session = self.caller

        # check for too many login errors too quick.
        if _throttle(session, maxlim=5, timeout=5*60,
                     storage=_LATEST_FAILED_LOGINS):
            # timeout is 5 minutes.
            session.msg("{RYou made too many connection attempts."
                        " Try again in a few minutes.{n")
            return

        args = self.args
        # extract quoted parts
        parts = [part.strip() for part in re.split(r"\"|\'",
                                                   args) if part.strip()]
        if len(parts) == 1:
            # this was (hopefully) due to no quotes being found or guest login
            parts = parts[0].split(None, 1)
            # Guest login
            if len(parts) == 1 and parts[0].lower() == "guest":
                enabled, new_player = create_guest_player(session)
                if new_player:
                    session.sessionhandler.login(session, new_player)
                if enabled:
                    return

        if len(parts) != 2:
            session.msg("\n\r Usage (without <>): continue <name> <password>\n"
                        "If you have spaces in your username, "
                        "enclose it in quotes.")
            return

        name, password = parts
        player = create_normal_player(session, name, password)
        if player:
            # actually do the login. This will call all other hooks:
            #   session.at_login()
            #   player.at_init()  # always called when object loaded from disk
            #   player.at_first_login()  # only once, for player-centric setup
            #   player.at_pre_login()
            #   player.at_post_login(session=session)
            session.sessionhandler.login(session, player)


class CmdUnconnectedNewGame(MuxCommand):
    """
    Create a new player account with the PokeMud meu system.

    Usage (at login screen):
      new game

    This triggers a system of menus which sets up a player/character.

    """
    key = "new game"
    aliases = ["new", "n"]
    locks = "cmd:all()"
    arg_regex = r"\s.*?|$"

    def func(self):
        session = self.caller

        # Character creation menu
        EvMenu(session, "commands.unloggedincmds", startnode="start_node",
               cmdset_mergetype="Replace", cmdset_priority=1,
               cmd_on_exit=CMD_LOGINSTART, node_formatter=null_node_formatter)


def start_node(caller):
    string = ("Hello there! Welcome to the world of POKEMON! My name is OAK!"
              "\nPeople call me the POKEMON PROF!")
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "start_node2"}
    return text, options


def start_node2(caller):
    string = ("This world is inhabited by creatures called POKEMON! For some "
              "people, POKEMON are pets. Others use them for fights. Myself..."
              "I study POKEMON as a profession.")
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "name_node"}
    return text, options


def name_node(caller):
    string = "First, what is your name?"
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "name_node2"}
    return text, options


def name_node2(session, raw_string):
    playername = raw_string
    playername = re.sub(r"\s+", " ", playername).strip()

    # sanity checks
    if not re.findall('^[\w. @+-]+$', playername) or not (
            0 < len(playername) <= 30):
        # this echoes the restrictions made by django's auth
        # module (except not allowing spaces, for convenience of
        # logging in).
        string = ("Your name must be a maximum of 30 characters or fewer.\n"
                  "Letters, spaces, digits and @/./+/-/_ only.\n"
                  "Press enter to try again!")
        text = poketext.bordertext(string, "Oak")
        options = {"key": "_default", "goto": "name_node"}
        return text, options

    if PlayerDB.objects.filter(username__iexact=playername):
        # player already exists (we also ignore capitalization here)
        string = ("That name already exists! "
                  "Lets try again!")
        text = poketext.bordertext(string, "Oak")
        options = {"key": "_default", "goto": "name_node"}
        return text, options

    # Check IP and/or name bans
    bans = ServerConfig.objects.conf("server_bans")
    if bans and (any(tup[0] == playername.lower() for tup in bans)
                 or
                     any(tup[2].match(session.address) for tup in bans if
                         tup[2])):
        # this is a banned IP or name!
        string = "{rYou have been banned and cannot continue from here." \
                 "\nIf you feel this ban is in error, please email an admin.{x"
        session.msg(string)
        session.sessionhandler.disconnect(session, "Good bye! Disconnecting.")
        return

    session.ndb._menutree.playername = playername

    string = ("Right! So your name is %s!" % playername)
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "password_node"}
    return text, options


def password_node(caller):
    string = "Next you'll need a password. What should your password be?"
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "password_node2"}
    return text, options


def password_node2(caller, raw_string):
    playerpassword = raw_string
    playerpassword = re.sub(r"\s+", " ", playerpassword).strip()

    # Password set up.
    if not re.findall('^[\w. @+-]+$', playerpassword) \
            or not (3 < len(playerpassword)):
        string = ("Your password should be longer than 3 characers.\n"
                  "Letters, spaces, digits and @\.\+\-\_ only.\n"
                  "For best security, make it longer than 8 characters.\n"
                  "Press enter to try again!")
        text = poketext.bordertext(string, "Oak")
        options = {"key": "_default", "goto": "password_node"}
        return text, options

    caller.ndb._menutree.playerpassword = playerpassword

    string = "...Erm, what was your password again?"
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "password_node3"}
    return text, options


def password_node3(caller, raw_string):
    if not (raw_string == caller.ndb._menutree.playerpassword):
        string = ("I don't think that's right!\n"
                  "Try again!")
        text = poketext.bordertext(string, "Oak")
        options = {"key": "_default", "goto": "password_node"}
        return text, options

    string = "That's right! I remember now!"
    text = poketext.bordertext(string, "Oak")
    options = {"key": "_default",
               "goto": "end_node"}
    return text, options


def end_node(caller):
    # everything's ok. Create the new player account.
    try:
        playername = caller.ndb._menutree.playername
        password = caller.ndb._menutree.playerpassword
        permissions = settings.PERMISSION_PLAYER_DEFAULT
        typeclass = characters.BaseCharacter

        new_player = _create_player(caller, playername, password, permissions)

        if new_player:
            if MULTISESSION_MODE < 2:
                default_home = ObjectDB.objects.get_id(settings.DEFAULT_HOME)
                _create_character(caller, new_player, typeclass,
                                  default_home, permissions)
            # tell the caller everything went well.
            string = ("%s!\n"
                      "Your very own POKEMON legend is about to unfold!\n"
                      "A world of dreams and adventures with POKEMON awaits! "
                      "Let's go!" % playername)
            text = poketext.bordertext(string, "Oak")
            options = {"key": "_default",
                       "goto": "end_node2"}
            return text, options

    except Exception:
        # We are in the middle between logged in and -not, so we have
        # to handle tracebacks ourselves at this point. If we don't,
        # we won't see any errors at all.
        caller.msg("An error occurred. Please e-mail an admin if the problem "
                   "persists.")
        logger.log_trace()


def end_node2(caller):
    playername = caller.ndb._menutree.playername
    password = caller.ndb._menutree.playerpassword

    caller.ndb._menutree.cmd_on_exit = lambda caller, menu: caller.execute_cmd(
        "connect " + playername + " " + password)

    text = ""

    return text, None

        
class CmdUnconnectedQuit(MuxCommand):
    """
    Default unlogged-in quit.
    Quit when in unlogged-in state

    Usage:
      quit

    We maintain a different version of the quit command
    here for unconnected players for the sake of simplicity. The logged in
    version is a bit more complicated.
    """
    key = "quit"
    aliases = ["q", "exit", "disconnect"]
    locks = "cmd:all()"

    def func(self):
        """Simply close the connection."""
        session = self.caller
        string = "Oak: Good bye! I'm disconnecting you now."
        session.sessionhandler.disconnect(session, string)


class CmdUnconnectedLook(MuxCommand):
    """
    Default unloged-in look.
    Display connect screen on connection or exit creation menu.

    Usage:
      look

    This is an unconnected version of the look command for simplicity.

    This is called by the server and kicks everything in gear. All it does
    is display the connect screen. Manually calling via look disabled.
    """
    key = CMD_LOGINSTART
    # aliases = ["look", "l"]
    locks = "cmd:all()"

    def func(self):
        """Show the connect screen."""
        connection_screen = ansi.parse_ansi(
            utils.random_string_from_module(CONNECTION_SCREEN_MODULE))
        if not connection_screen:
            connection_screen = ("No connection screen found. "
                                 "Please contact an admin.")
        self.caller.msg(connection_screen)


class CmdUnconnectedHelp(MuxCommand):
    """
    get help when in unconnected-in state

    Usage:
      help

    This is an unconnected version of the help command,
    for simplicity. It shows a pane of info.
    """
    key = "help"
    aliases = ["h", "?"]
    locks = "cmd:all()"

    def func(self):
        """Shows help"""

        string = \
            """ \
Welcome to the exciting world of Pokemon! You are not yet logged into the game.
Commands available at this point:

{wnew game{n - start a new Pokemon adventure
{wcontinue{n - continue with an existing Pokemon journey
{whelp{n - show this help
{wencoding{n - change the text encoding to match your client
{wscreenreader{n - make the server suitable for use with screen readers
{wquit{n - abort the connection \
"""
        self.caller.msg(poketext.nobordertext(string, "Oak"))

