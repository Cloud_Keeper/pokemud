# encoding=utf-8
"""
This EvForm is used by the CmdTrainer in commands/charactercmds.CmdTrainer
It provides the player with a sort of character sheet following the aesthetic
of the Pokeon gameboy games.
"""

FORMCHAR = "x"

FORM = """
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃   NAME/   xxxxxAxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃   MONEY/  ₱xxxxBxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃   TIME/   xxxxxCxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃                 ●BADGES●                 ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃   1.        2.        3.         4.      ┃
                ┃       xDx       xEx       xFx        xGx ┃
                ┃   5.        6.        7.         8.      ┃
                ┃       xHx       xIx       xJx        xKx ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛\
"""


