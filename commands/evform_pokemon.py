# encoding=utf-8
"""
This EvForm is used by the CmdTrainer in commands/charactercmds.CmdTrainer
It provides the player with a sort of character sheet following the aesthetic
of the Pokeon gameboy games.
"""

FORMCHAR = "x"

FORM = """
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃  No. 001      Bulbasaurxxxxxxxxxxxxxxxx  ┃
                ┃               xxxxxCxxxxxxxxxxxxxxxxxxx  ┃
                ┃               HT xxDxxxxxxxxxxxxxxxxxxx  ┃
                ┃               WT xxExxxxxxxxxxxxxxxxxxx  ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃  xxxxxxxxxxxxxxxxxxFxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛\


 No. 001    BULBASUAR
           ◓══════════◓════════════════════◓
LVL: 5     ║ ATTACK   ║                    ║
HP :100/100║       10 ║ CONFUSE RAY        ║
STATUS/OK  ║ DEFENCE  ║          PP  10/10 ║
TYPE1/     ║       10 ║ NIGHT SHADE        ║
  GRASS    ║ SPEED    ║          PP  10/10 ║
TYPE2/     ║       10 ║ HYPNOSIS           ║
  POISON   ║ SP ATT   ║          PP  10/10 ║
IDNO/      ║       10 ║ DREAM EATER        ║
 1111111   ║ SP DEF   ║          PP  10/10 ║
           ║       10 ║                    ║
           ◓══════════◓════════════════════◓
"""


