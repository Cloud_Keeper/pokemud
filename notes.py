# encoding=utf-8
class TODO ():
    """
    -Complete Batchcode
        *Pallet Town Tag on rooms.
    -Equipment poses shows in character description.
    -Give, drop, etc multiple items.
    -Surf exits
    -radio objects and commands
    -Battle System
    - extend CmdTrainer
    - Complete Menu Commands.
    - CmdInventory can be simplified
    - Look back over the create Pokemon command/code.
    """
    pass

class Questions ():
    """
    Why do all the ainneve objects have class attributes which get converted
    to database attributes in at create? Create functions don't allow them
    being changed. and could just set them in on_create?
    """
    pass

class NotesCombat ():
    """
    Combat:
    commands - Pokemon name command for combat (attack commands only, maybe
    the out of combat commands with "Not whilst hes fighting" type message)
    -say
    -pose
    -emote
    -help
    -run
    """
    pass

class NotesPokemon ():
    """
    Generate a bunch of pokemon that will be used for a given area. These
    pokemon don't level up but they retain their behvaioural and decision
    behaviours. That way the pokemon you get appear real and diverse rather
    than randomly generated. When caught another blank slate is generated.
    Takes from pool both in random attacks and wandering mobs.

    individual character - love=# hate=# anger=#...
    humans in general ""
    individual pokemon ""
    species of pokemon in general ""
    """
    pass

class NotesRooms ():
    """
    if someone in room start flavour text, if no one cancel - subscribe to
    ticker on object enter room, add its flavour text.

    Zones –determine which pokemon present
    Weather based on position of legendary birds.
    Night/day cycle.
    Descriptions must work when in photo form.
    Flavour text based on weather/npcs and objects in the room.
    Option to turn off flavour text.

    """
    pass

class NotesItems ():
    """
    If player has 10 potions, it only really has one with a number value of 10

    ** This would require a total rewrite of everything. Equipment handelr,
    basic evennia interactions. Poses. Everything.

    There would have to be a lot of deleting and creating of objects.

    I've abandoned this. Instead I've decided to just have the inventory
    and looks calls parse the items into numbered lists.

    Maybe something for the future if there is enough gain.
    -----------------------------------------------------

    could automatically make pose "lies on table" if you do command
    "drop cap on table"

    -------------------------------------------------------

    Use command - Currently only accesses objects in inventory. Could extend
    it if required to, if not finding anything in inventory, then searching
    outside to allow use of objects in room.

    When it comes to restricting use of objects. If the use command was on
    the object we could restrict who the use is available to if we were only
    allowing use of items in your inventory then just searching if in
    contents (like equip) would be okay but if we are doing a master use
    command then there will need to be a lock.

    ---------------------------------------------------------
    If they items have a at_use command that does the action.
    Like a consumables have the at use which has the effect
    equipment have the at_use hook which does the equipping.
    Then the commands just have to make sure the arguments are valid
    and call the hooks.

    Still consider moving the equipping part to the obj hook.

    *I've tried this by returning a tuple (Bool if worked, Message) but the
    message requires the inclusion of variables inconsistantly. I guess we
    could move the command used to the hook as well, that way it could
    determine whether it was initiated by a command or in code but any
    code could just initiate the command then check if it worked.

    If not moving it to the hook, consider having an attribute on the command
    for silent action. It's what Ainneve does with it's equip commands.
    ---------------------------------------------------------

    Dynamically determine an or a and plurals

    extend get command. "get hat" takes 1 hat. "get 10 hat" gets 10 hats.
    will have to check for plural

    -------------------------------------------------------

    We could have classes of items
    Potion = potion, hyper potion. They all have similar similar use or set
    out individually in at_use hook.
    """
    pass

class NotesRadio ():
    """
    Add view locks to channels so players can't see radio channels.
    """
    pass

class NotesNPC ():
    """
    Need to redo conversation storing conversation dictionaries on the NPC.
    """
    pass

class NotesClothing ():
    """
    Could use SDESC for things like "Muddy *obj name*"

    -----------------------------------
    Slot for pokeballs. But it can be anything:
    Belt
    Sash
    Bandolier

    """
    pass