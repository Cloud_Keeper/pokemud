"""
Input functions

Input functions are always called from the client (they handle server
input, hence the name).

This module is loaded by being included in the
`settings.INPUT_FUNC_MODULES` tuple.

All *global functions* included in this module are considered
input-handler functions and can be called by the client to handle
input.

An input function must have the following call signature:

    cmdname(session, *args, **kwargs)

Where session will be the active session and *args, **kwargs are extra
incoming arguments and keyword properties.

A special command is the "default" command, which is will be called
when no other cmdname matches. It also receives the non-found cmdname
as argument.

    default(session, cmdname, *args, **kwargs)

"""

# import the contents of the default inputhandler_func module
#from evennia.server.inputfuncs import *


# def oob_echo(session, *args, **kwargs):
#     """
#     Example echo function. Echoes args, kwargs sent to it.
#
#     Args:
#         session (Session): The Session to receive the echo.
#         args (list of str): Echo text.
#         kwargs (dict of str, optional): Keyed echo text
#
#     """
#     session.msg(oob=("echo", args, kwargs))
#
#
# def default(session, cmdname, *args, **kwargs):
#     """
#     Handles commands without a matching inputhandler func.
#
#     Args:
#         session (Session): The active Session.
#         cmdname (str): The (unmatched) command name
#         args, kwargs (any): Arguments to function.
#
#     """
#     pass

# from django.conf import settings
# from evennia.commands.cmdhandler import cmdhandler
# from evennia.utils.logger import log_file

# # always let "idle" work since we use this in the webclient
# _IDLE_COMMAND = settings.IDLE_COMMAND
# _IDLE_COMMAND = (_IDLE_COMMAND, ) if _IDLE_COMMAND == "idle" else (_IDLE_COMMAND, "idle")


# def text(session, *args, **kwargs):
    # """
    # Copy and paste from Evennia Vanilla but adding logging.
    # REPLCED BY DATA_IN SETTING ADDED IN UPDATE
    
    # Main text input from the client. This will execute a command
    # string on the server.
    # Args:
        # text (str): First arg is used as text-command input. Other
            # arguments are ignored.
    # """
    # #from evennia.server.profiling.timetrace import timetrace
    # #text = timetrace(text, "ServerSession.data_in")

    # text = args[0] if args else None

    # #explicitly check for None since text can be an empty string, which is
    # #also valid
    # if text is None:
        # return
        
    # # this is treated as a command input
    # # handle the 'idle' command
    # if text.strip() in _IDLE_COMMAND:
        # session.update_session_counters(idle=True)
        # return
        
    # if session.player:
        # # Custom log code
        # log_file(text, filename=session.player.key + ".log")
        
        # # nick replacement
        # puppet = session.puppet
        # if puppet:
            # text = puppet.nicks.nickreplace(text,
                          # categories=("inputline", "channel"), include_player=True)
        # else:
            # text = session.player.nicks.nickreplace(text,
                        # categories=("inputline", "channel"), include_player=False)
    # # Custome log code
    # else:
        # log_file(text, filename=str(session.sessid) + ".log")
        
    # kwargs.pop("options", None)
    # cmdhandler(session, text, callertype="session", session=session, **kwargs)
    # session.update_session_counters()