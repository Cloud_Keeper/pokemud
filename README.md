#Welcome to PokeMud#

Pokemon Red/Blue replicated in a MUD.

![Untitled.png](https://bitbucket.org/repo/Mo6EKr/images/1684087439-Untitled.png)

This directory is an [Evennia](http://www.evennia.com/) game directory. Install Evennia as per the [official instructions](https://github.com/evennia/evennia/wiki/Getting-Started) then clone locally, migrate and start with `evennia start`.

## Interesting Code ##
This is a work of progress but if your taking a peek the following might be of interest to you:
### \Commands ###
* charactercmds.py : This has a simple character sheet command which uses EvTable and a Talk command which uses EvMenu and draws from a centralised conversation module.
* unloggedincmds.py : Contains a simple completely menu based character creation system using EvMenu
### \Typeclasses ###
* characters.py : Sets up characters with a range game values and their own room.
* exits.py : Contains a varied exit which sends players to the room stored on their character.
### \World ###
* batchcode : Contains a number of mature examples of batchcode to create rooms, objects and npcs with their accompanying descriptions and details.
* conversation : The centralised repository of all NPC dialogue.

## Goal ##
Replicate the Pokemon Red/Blue experience in MUD form. Once the base game is completed, expand on the concepts taking advantage of the text based medium.
### Pokemon ###
* Goal: 151 Pokemon.
    Introduce the original cast with appropraite descriptions.
* Stretch Goal: Expand the AI.
    A Black and White (Lionhead) -esque AI determining how the Pokemon reacts to you and it's surroundings. Interacting with your Pokemon will be just as interesting as battling.
### Combat ###
* Goal: The classic battle system.
    Recreate the battle system from Red and Blue.
* Stretch Goal: Theatrical battle.
    Whilst the battle system and mechanics stays largely the same, expand on what is provided to the player with flavour text. In between attacks the pokemon will jostle and move, reacting to each other. What players see will be affected by visibility depending on the weather and the dust stirred from battle. Depending on location, reactions from the crowd and announcers comments.
### NPCs ###
* Goal: Press A for dialogue.
    All NPCs in place with their in-game dialogue
* Stretch Goal: Extended Conversation System.
    A key-word based system where conversation is dynamic based on the NPCs dynamically gained knowledge of the world and people around them.